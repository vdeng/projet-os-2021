.PHONY: all clean kernel qemu

all:
	bash iso.sh

kernel:
	bash build.sh

clean:
	bash clean.sh

qemu:
	bash qemu.sh
