# Projet OS 2021

Pad : https://www.eleves.ens.fr/pads/p/H08MX6-8bz3HlRwvH6jg

Le squelette du projet provient de https://wiki.osdev.org/Meaty_Skeleton.

Le projet nécessite le compilateur croisé obtenu en suivant les instructions à l'adresse https://wiki.osdev.org/GCC_Cross-Compiler. Il nécessite également d'avoir installé xorriso, GRUB, make et éventuellement qemu.

Pour obtenir ces dernières dépendances, sur un système Ubuntu récent, taper `sudo apt-get install xorriso grub-common make qemu-system-x86`.

## Compilation

Taper `make` pour créer un fichier .iso démarrable.

Taper `make clean` pour effacer les fichiers résultant de la compilation.

Taper `make qemu` pour compiler le projet et lancer qemu avec le résultat.
