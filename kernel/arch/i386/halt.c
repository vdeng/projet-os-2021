#include <halt.h>

void halt() {
    asm volatile ("hlt");
}