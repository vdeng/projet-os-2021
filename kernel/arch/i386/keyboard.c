#include <io.h>
#include <keyboard.h>
#include <stdint.h>
#include <stdbool.h>
#include <halt.h>

// Source : https://wiki.osdev.org/PS/2_Keyboard
struct kbd_event kbd_last_event;
bool kbd_has_key;
uint8_t kbd_scan_buf[9];
int kbd_buf_sz;

#define KEY(row, col) ((row << 5) + col) // 32 colonnes, bcp de lignes

const int scancode_table[256] = {
    //0: -, F9, -, F5, F3, F1, F2, F12, -, F10, F8, F6, F4, tab, `, -
    -1, KEY(0,9), -1, KEY(0,5), KEY(0,4), KEY(0,1), KEY(0,2), KEY(0,12), -1, KEY(0,10), KEY(0,8), KEY(0,6), KEY(0,4), KEY(2,0), KEY(1,0), -1,
    //1: -, left alt, left shift, -, left ctrl, Q, 1, -, -, -, Z, S, A, W, 2, -
    -1, KEY(5,2), KEY(4,0), -1, KEY(5,0), KEY(2, 1), KEY(1, 1), -1, -1, -1, KEY(4,1), KEY(3,2), KEY(3,1), KEY(2,2), KEY(1,2), -1,
    //2: -, C, X, D, E, 4, 3, -, -, space, V, F, T, R, 5, -
    -1, KEY(4, 3), KEY(4, 2), KEY(3, 3), KEY(2, 3), KEY(1, 4), KEY(1, 3), -1, -1, KEY(5, 3), KEY(4, 4), KEY(3, 4), KEY(2, 5), KEY(2, 4), KEY(1, 5), 0,
    //3: -, N, B, H, G, Y, 6, -, -, -, M, J, U, 7, 8, -
    -1, KEY(4, 6), KEY(4, 5), KEY(3, 6), KEY(3, 5), KEY(2, 6), KEY(1, 6), -1, -1, -1, KEY(4, 7), KEY(3, 7), KEY(2, 7), KEY(1,7), KEY(1, 8), 0,
    //4: -, ,, K, I, O, 0, 9, -, -, ., /, L, ;, P, '-', -
    -1, KEY(4, 8), KEY(3, 8), KEY(2, 8), KEY(2, 9), KEY(1, 10), KEY(1, 9), -1, -1, KEY(4, 9), KEY(4, 10), KEY(3, 9), KEY(3, 10), KEY(2, 10), KEY(1, 11), 0,
    //5: -, -, ', -, [, =, -, -, capslock, right shift, enter, ], -, \, -, -
    -1, -1, KEY(3, 11), -1, KEY(2, 11), KEY(1, 12), -1, -1, KEY(3, 0), KEY(4, 11), KEY(3, 12), KEY(2, 12), -1, KEY(2, 13), -1, 0,
    //6: -, -, -, -, -, -, backspace, -, -, 1num, -, 4num, 7num, -, -, -
    -1, -1, -1, -1, -1, -1, KEY(1, 13), -1, -1, KEY(6, 1), -1, KEY(6, 4), KEY(6, 7), -1, -1, -1,
    //7: 0num, .num, 2num, 5num, 6num, 8num, escape, numlock, F11, +num, 3num, -num, *num, 9num, scrollock, -
    KEY(6,0), KEY(6,10), KEY(6,2), KEY(6,5), KEY(6,6), KEY(6,8), KEY(0,0), KEY(6,11), KEY(0,11), KEY(6,12), KEY(6,3), KEY(6,13), KEY(6, 14), KEY(6,9), KEY(0,14), -1,
    //8: -, -, -, F7, ---
    -1, -1, -1, KEY(0, 7), -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    //9ABCD : rien
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    //E : multimédia et alt/control droit, GUI, apps, power, /num, enternum, end, home, etc.
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
};

const int e0_scancode_table[256] = {
    //0: rien
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    //1: WWW search, right alt, -, -, right ctrl, prev track, -, -, www favourites, -, -, -, -, -, -, left GUI
    -1, KEY(5, 4), -1, -1, KEY(5, 7), -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, KEY(5, 1),
    //2: WWW refresh, vol down, -, mute, -, -, -, right GUI, WWW stop, -, -, calculator, -, -, -, apps
    -1,-1,-1,-1,-1,-1,-1,KEY(5,5),-1,-1,-1,-1,-1,-1,-1,-1,
    //3: WWW forward, -, WWW home, -, playpause, -, -, power, WWW back, -, WWW home, stop, -, -, -, sleep
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    //4: my computer, -, -, -, -, -, -, -, email, -, /num, -, -, next track, -, -
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, KEY(6,15), -1, -1, -1, -1, -1
    //5: media select, -, -, -, -, -, -, -, -, -, enternum, -, -, -, wake, -
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,KEY(6,17),-1,-1,-1,-1,-1,
    //6: -, -, -, -, -, -, -, -,   -, end, -, cursor left, home, -, -, -
    -1,-1,-1,-1,-1,-1,-1,-1,-1,KEY(7,4),-1,KEY(7,7),KEY(7,1),-1,-1,-1,
    //7: insert, delete, cursor down, -, cursor right, cursor up, -, -, -, -, page down, -, -, page up, -, -
    KEY(7,0), KEY(7,3), KEY(7,8), -1, KEY(7,9), KEY(7,6), -1, -1, -1, -1, KEY(7,5), -1, -1, KEY(7, 2), -1, -1,
    //89ABCDE : rien
    //F : relâchement
};

const char char_table[2][2][7][32] = { // shift, altgr, row, col
    { // no shift
        { // no altgr
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\t', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\\', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', ' ', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '\0', '+', '-', '*', '/',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},
        },
        { // altgr
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
        }
    },
    { // shift
        { // no altgr
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '|', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', ' ', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '\0', '+', '-', '*', '/',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},
        },
        { // altgr
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
            {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
            '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',},
        }
    }
};

void init_kbd() {
    kbd_buf_sz = 0;
    kbd_has_key = false;
    struct kbd_event init_event = { .charcode = '\0', .keycode = -1, .pressed = false, .lctrl = false, .lalt = false, .lshift = false, .rctrl = false, .ralt = false, .rshift = false, .altgr = false, .capslock = false, .numlock = false, .scrolllock = false };
    kbd_last_event = init_event;
    outb(0x64, 0x20); // Request controller configuration byte
    while(inb(0x64) & 0b00000001) {} // Wait for response
    uint8_t config = inb(0x60);
    config &= 0b10111111; // Set translation bit to 0
    outb(0x64, 0x60); // Write config byte
    while((inb(0x64) & 0b00000010) != 0) {}
    outb(0x60, config);
}

void kbd_process_scancode(uint8_t scancode) {
    if(kbd_buf_sz == 0 && (scancode == 0xE0 || scancode == 0xE1 || scancode == 0xF0)) {
        kbd_scan_buf[kbd_buf_sz++] = scancode;
    }
    else if(kbd_buf_sz == 0 || (kbd_buf_sz == 1 && kbd_scan_buf[0] == 0xF0)) {
        int key = scancode_table[scancode];
        int row = key >> 5;
        int col = key & 0b11111;
        bool is_shift = ((kbd_last_event.lshift || kbd_last_event.rshift) + kbd_last_event.capslock) & 1;
        kbd_last_event.charcode = char_table[is_shift][kbd_last_event.altgr][row][col];
        kbd_last_event.keycode = key;
        bool pressed = kbd_buf_sz == 0;
        kbd_last_event.pressed = pressed;
        if(pressed && key == KEY(3, 0))
            kbd_last_event.capslock = !kbd_last_event.capslock;
        else if(pressed && key == KEY(6,11))
            kbd_last_event.numlock = !kbd_last_event.numlock;
        else if(pressed && key == KEY(0,14))
            kbd_last_event.scrolllock = !kbd_last_event.scrolllock;
        else if(key == KEY(4, 0))
            kbd_last_event.lshift = pressed;
        else if(key == KEY(5,0))
            kbd_last_event.lctrl = pressed;
        else if(key == KEY(5,2))
            kbd_last_event.lalt = pressed;
        else if(key == KEY(4,11))
            kbd_last_event.rshift = pressed;
        else if(key == KEY(5,4))
            kbd_last_event.ralt = pressed;
        else if(key == KEY(5, 7))
            kbd_last_event.rctrl = pressed;
        kbd_has_key = true;
        kbd_buf_sz = 0;
    }
    else if(kbd_buf_sz == 1 && kbd_scan_buf[0] == 0xE0  && (scancode == 0xF0 || scancode == 0x12)) // Relâchement, ou print screen pressed
        kbd_scan_buf[kbd_buf_sz++] = scancode;
    else if((kbd_buf_sz == 1 && kbd_scan_buf[0] == 0xE0) || (kbd_buf_sz == 2 && kbd_scan_buf[0] == 0xE0 && kbd_scan_buf[1] == 0xF0 && scancode != 0x7C)) {
        int key = e0_scancode_table[scancode];
        int row = key >> 5;
        int col = key & 0b11111;
        bool is_shift = ((kbd_last_event.lshift || kbd_last_event.rshift) + kbd_last_event.capslock) & 1;
        kbd_last_event.charcode = char_table[is_shift][kbd_last_event.altgr][row][col];
        kbd_last_event.keycode = key;
        bool pressed = kbd_buf_sz == 1;
        kbd_last_event.pressed = pressed;
        if(key == KEY(5,4))
            kbd_last_event.ralt = pressed;
        else if(key == KEY(5,7))
            kbd_last_event.rctrl = pressed;
        kbd_has_key = true;
        kbd_buf_sz = 0;
    }
    else if(kbd_buf_sz >= 1 && kbd_scan_buf[0] == 0xE1) { // pause
        kbd_scan_buf[kbd_buf_sz++] = scancode;
        if(kbd_buf_sz == 8) {
            int key = KEY(0,15);
            kbd_last_event.pressed = true;
            kbd_last_event.charcode = '\0';
            kbd_last_event.keycode = key;
            kbd_has_key = true;
            kbd_buf_sz = 0;
        }
    }
    else if(kbd_buf_sz >= 2 && kbd_scan_buf[0] == 0xE0 && kbd_scan_buf[1] == 0x12) {
        kbd_scan_buf[kbd_buf_sz++] = scancode;
        if(kbd_buf_sz == 4) {
            kbd_last_event.keycode = KEY(0,13);
            kbd_last_event.charcode = '\0';
            kbd_last_event.pressed = true;
            kbd_has_key = true;
            kbd_buf_sz = 0;
        }
    }
    else if(kbd_buf_sz >= 3 && kbd_scan_buf[0] == 0xE0 && kbd_scan_buf[1] == 0xF0 && kbd_scan_buf[2] == 0x7C) {
        kbd_scan_buf[kbd_buf_sz++] = scancode;
        if(kbd_buf_sz == 6) {
            kbd_last_event.keycode = KEY(0,13);
            kbd_last_event.charcode = '\0';
            kbd_last_event.pressed = false;
            kbd_has_key = true;
            kbd_buf_sz = 0;
        }
    }
    else {
        kbd_buf_sz = 0; // FAIL
    }
}

struct kbd_event kbd_poll_event() {
    while(!kbd_has_key)
        halt();
    kbd_has_key = false;
    return kbd_last_event;
}