#ifndef FILESYSTEM_H
#define FILESYSTEM_H 1

#define TAILLE_DOSSIER 10
#define TAILLE_NOM 20 /*taille des chaînes de caractères utilisées pour les noms de fichier*/
#define TAILLE_FICHIER 100 

enum type_fichier { Fichier, Dossier };
typedef struct fichier_s {
    enum type_fichier type;
    char nom[TAILLE_NOM];
    struct fichier_s* pere;
    union {
        struct fichier_s* sous_dossier[TAILLE_DOSSIER]; // à changer ?
        char contenu[TAILLE_FICHIER];
    };
} fichier ;

#endif
