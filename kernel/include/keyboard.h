#ifndef _KEYBOARD_H
#define _KEYBOARD_H 1
#include <stdbool.h>
#include <stdint.h>

#define KBD_KEY(row, col) ((row << 5) + col) // 32 colonnes, bcp de lignes

#define KBD_ENTER KBD_KEY(3,12)
#define KBD_BACKSPACE KBD_KEY(1,13)
#define KBD_DELETE KBD_KEY(7,3)

struct kbd_event {
    char charcode;
    int keycode;
    bool pressed;
    bool lctrl, lalt, lshift, rctrl, ralt, rshift, altgr;
    bool capslock, scrolllock, numlock;
};

struct kbd_event kbd_poll_event();
void kbd_process_scancode(uint8_t scancode);
extern struct kbd_event kbd_last_event;
extern bool kbd_has_key;
extern uint8_t kbd_scan_buf[9];
extern int kbd_buf_sz;

#endif