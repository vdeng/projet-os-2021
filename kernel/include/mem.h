#ifndef KERNEL_MEM_H
#define KERNEL_MEM_H 1

#include <stdbool.h>
#include <stdint.h>
#define PAGE_SIZE 4096

typedef struct page_t {
    bool allocated;
    struct page_t *next;
} page_t;

typedef struct {
    page_t *begin;
    page_t *end;
} page_list_t;

void append_page(page_list_t *list, page_t *page);
page_t* pop_page(page_list_t *list);
void init_page_list(uint32_t end);
void* alloc_page();
void free_page(void* addr);
void page_test();

#endif