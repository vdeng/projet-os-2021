#ifndef MICROKERNEL_H
#define MICROKERNEL_H 1

#define MAX_TIME_SLICES 5
#define MAX_PRIORITY 15
#define NUM_PROCESSES 32
#define NUM_CHANNELS 128
#define NUM_REGISTERS 5

enum Process_state1 { Free, BlockedWriting, BlockedReading, Waiting, Runnable, Zombie };
typedef struct {
    enum Process_state1 process_state_type;
    union {
        int chanid; int chanidlist[NUM_REGISTERS - 1]; /*tableau qui simule une liste*/
    };
} Process_state;

typedef struct {
    int parent_id; 
    Process_state state; 
    int slices_left; 
    int saved_context[NUM_REGISTERS];
} Process;

enum Channel_state1 { Unused, Sender, Receivers };
typedef struct {
    enum Channel_state1 channel_state_type;
    union {
        int sender_pid_priority_value[3];
        int receivers_pid_prio[NUM_PROCESSES][2]; /*simule une liste de couples*/
    };
} Channel_state;

typedef struct {
    int curr_pid;
    int curr_prio;
    int registers[NUM_REGISTERS];
    Process processes[NUM_PROCESSES]; 
    Channel_state channels[NUM_CHANNELS];
    int runqueues[MAX_PRIORITY+1][NUM_PROCESSES]; /* Les priorités vont de 0 à MAX_PRIORITY, d'où le +1.
    runqueues[prio] est un tableau qui simule une liste contenant les identifiants des processus de priorité prio.*/
    int position; /* J'ajoute cet entier, qui sert à simuler les runqueues avec des tableaux.
    Pour pouvoir traiter les processus par priorité décroissante, en prenant à tour de rôle les processus
    d'égale priorité, il faut retenir où on en est dans le tableau. Cf fonction choose_processus.*/
    /* Si on change l'ordonnancement, on pourra supprimer ce champ. Il n'intervient que dans init() et choose_processus().*/
    int count; /* J'ajoute aussi cet entier pour l'ordonnancement (algorithme 2). Il indique le nombre de
               fois où d'affilé où le processus courant a été choisi.*/
} Kernel_state;

typedef enum { Timer, SysCall } Event;

enum Syscall1 { Send, Receive, Fork, Wait, Exit, NewChannel, Invalid };
typedef struct {
    enum Syscall1 syscall_type;
    union {
        int send_val[2]; int receive_val[NUM_REGISTERS - 1]; int fork_val[NUM_REGISTERS-1];
    /* Dans fork_val, le premier élément est la nouvelle priorité, les autres les valeurs pour initialiser les registres du fils.*/
    /* Dans le code OCaml, il y avait 4 à la place de NUM_REGISTERS - 1, mais ce 4 était à comprendre comme NUM_REGISTERS - 1.
    J'ai donc adapté le code au cas où on voudrait changer le nombre de registres.*/
    };
} Syscall;


extern Kernel_state kernel;

void init();
void transition(Event ev, Kernel_state* kernel);

#endif