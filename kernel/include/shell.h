#ifndef KERNEL_SHELL_H
#define KERNEL_SHELL_H

#include <filesystem.h>

void shell();

void entree_clavier(char entree[]);

int longueur(char chaine[]);

int sont_egales(char chaine1[],char chaine2[]);

int prochaine_position(char chaine[],int d,char c);

char *lire(char chaine[],int d,int f);

fichier* search_child(fichier* dir, char child[]);

fichier* follow_path(char chemin[]);

void echo(char chaine[]);

void ls(char chemin[]);

void path(fichier* f);

void pwd();

void cd(char chemin[]);

int nom_valide(fichier f,char nom[]);

int emplacement_libre(fichier f);

void touch(char chemin[]);

void mkdir(char chemin[]);

int dossier_non_vide(fichier* f);

void rm(fichier* f);

void rm_fichier(char chemin[]);

void rm_r(char chemin[]);

void add(char chaine[]);

int find_int(char chaine[]);

void cut(char chaine[]);

void cp(fichier* fi, fichier* da);

void cp_fichier(char chaine[]);

void cp_d(char chaine[]);

void mv_fichier(char chaine[]);

void mv_d(char chaine[]);

#endif
