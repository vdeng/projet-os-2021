#ifndef _KERNEL_TTY_H
#define _KERNEL_TTY_H 1

#include <stddef.h>
#include <stdint.h>

void terminal_initialize(void);
void terminal_putchar(char c);
void terminal_write(const char* data, size_t size);
void terminal_writestring(const char* data);
void terminal_erase_curr();
void terminal_set_position(size_t row, size_t column);
size_t terminal_get_row();
size_t terminal_get_column();
uint16_t terminal_get_cursor_position(void);
void terminal_disable_cursor();
void terminal_enable_cursor(uint8_t cursor_start, uint8_t cursor_end);

#endif
