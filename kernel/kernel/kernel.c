#include <stdio.h>
#include <stdint.h>

#include <multiboot.h>
#include <halt.h>
#include <mem.h>
#include <microkernel.h>
#include <tty.h>
#include <shell.h>
#include <keyboard.h>

extern void* endKernel;


uint32_t get_end_mem(multiboot_info_t* mbd) {
	if((mbd->flags >> 6) & 1) {
		for(uint32_t* addr = (uint32_t*) (mbd->mmap_addr+sizeof(uint32_t)); (uint32_t)addr < mbd->mmap_addr + mbd->mmap_length; addr += addr[-1]/4+1) {
			uint32_t base_low = addr[0], base_high = addr[1], length_low = addr[2], length_high = addr[3];;
			if(base_high == 0 && base_low <= (uint32_t) (&endKernel) && length_high == 0 && (base_low + length_low) > (uint32_t) (&endKernel))
				return base_low + length_low;
		}
	}
	return 0;
}

// Pour débogage/tests
uint32_t print_mmap(multiboot_info_t* mbd) {
	uint32_t ret = 0;
	if(mbd->flags & 1) {
		printf("Contiguous memory: lower %d KiB, upper %d KiB\n", mbd->mem_lower, mbd->mem_upper);
	}
	if((mbd->flags >> 6) & 1) {
		printf("Complete memory map: length %d %x\n", mbd->mmap_length, mbd->mmap_length);
		for(uint32_t* addr = (uint32_t*) (mbd->mmap_addr+sizeof(uint32_t)); (uint32_t)addr < mbd->mmap_addr + mbd->mmap_length; addr += addr[-1]/4+1) {
			uint32_t base_low = addr[0], base_high = addr[1], length_low = addr[2], length_high = addr[3];;
			printf("Base address: 0x%x high %x low Length: 0x%x high 0x%x low ", addr[1], addr[0], addr[3], addr[2]);
			if(addr[4] == 1) {
				printf("available");
			}
			else if(addr[4] == 3) {
				printf("ACPI");
			}
			else if(addr[4] == 5) {
				printf("defective");
			}
			else {
				printf("reserved");
			}
			printf("\n");
			if(base_high == 0 && base_low <= (uint32_t) (&endKernel) && length_high == 0 && (base_low + length_low) > (uint32_t) (&endKernel))
				ret = base_low + length_low;
		}
	}
	return ret;
}

void kernel_main(multiboot_info_t* mbd) {
	terminal_initialize();
	terminal_enable_cursor(0, 15);
	printf("This is MyOS, built on %s.\n", __DATE__);
	uint32_t end_mem = get_end_mem(mbd);
	if(end_mem == 0) {
		printf("Not enough RAM detected, exiting\n");
		return;
	}
	init_page_list(end_mem);
	printf("Initializing kernel...\n");
	init();
	printf("Done initializing.\n\n");
	shell();
}
