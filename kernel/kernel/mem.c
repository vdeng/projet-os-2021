// Inspiré de https://jakesandler.developpez.com/contruire-systeme-d-exploitation-raspberry-pi/#L4-2
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <mem.h>

void append_page(page_list_t *list, page_t *page) {
    if(list->end == NULL) {
        list-> begin = page;
        list->end = page;
        page->next = NULL;
    }
    else {
        list->end->next = page;
        list->end = page;
        page->next = NULL;
    }
}

page_t* pop_page(page_list_t *list) {
    if(list->begin == NULL)
        return NULL;
    page_t *ret = list->begin;
    list->begin = list->begin->next;
    ret->next = NULL;
    return ret;
}

page_list_t page_list;
page_t *page_array;
uint32_t end_addr;
extern void* endKernel;

void init_page_list(uint32_t end) {
    end_addr = end;
    size_t num_pages = end / PAGE_SIZE;
    printf("Initializing %d pages...\n", num_pages);
    page_array = (page_t*) &endKernel;
    page_list.begin = NULL;
    page_list.end = NULL;
    for(size_t i = 0; i < num_pages; i++) {
        page_t page = {false, NULL};
        page_array[i] = page;
    }
    size_t num_kernel_pages = (uint32_t)(page_array + num_pages) / PAGE_SIZE + 1;
    for(size_t i = 0; i < num_kernel_pages; i++) {
        page_array[i].allocated = true;
    }
    for(size_t i = num_kernel_pages; i < num_pages; i++)
        append_page(&page_list, &page_array[i]);
}

void* alloc_page() {
    page_t * page = pop_page(&page_list);
    if(page == NULL)
        return NULL;
    page->allocated = true;
    uint32_t page_id = (uint32_t) (page - page_array);
    uint32_t page_addr = PAGE_SIZE * page_id;
    return (void*) page_addr;
}

void free_page(void* addr) {
    uint32_t page_id = ((uint32_t) addr)/PAGE_SIZE;
    page_t * page = page_array + page_id;
    if(!page->allocated)
        return;
    page->allocated = false;
    append_page(&page_list, page);
}

void page_test() {
    uint32_t nb = 2*end_addr/PAGE_SIZE;
    for(uint32_t i = 0; i < 2*nb; i++) {
        uint32_t* page = (uint32_t*) alloc_page();
        uint32_t* page2 = (uint32_t*) alloc_page();
        if(page == NULL || page2 == NULL)
            printf("argh\n");
        page[0] = 42;
        page2[1] = 42;
        free_page(page);
        free_page(page2);
        if(i % 10000 == 0) {
            printf("%d/%d...\n", i, 2*nb);
        }
    }
}