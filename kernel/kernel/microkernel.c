#include <stdio.h>
#include <stdlib.h>
#include <microkernel.h>

Kernel_state kernel;

Syscall decode(Kernel_state kernel) {
    Syscall appel_syst;
    switch (kernel.registers[0]) {
    case 0: appel_syst.syscall_type = NewChannel; break;
    case 1:
        appel_syst.syscall_type = Send;
        appel_syst.send_val[0] = kernel.registers[1];
        appel_syst.send_val[1] = kernel.registers[2];
        break;
    case 2:
        appel_syst.syscall_type = Receive;
        for (int i = 0; i < NUM_REGISTERS - 1; i++) { appel_syst.receive_val[i] = kernel.registers[i + 1]; };
        break;
    case 3:
        appel_syst.syscall_type = Fork;
        for (int i = 0; i < NUM_REGISTERS - 1; i++) {
            appel_syst.fork_val[i] = kernel.registers[i + 1];
        };
        break;
    case 4: appel_syst.syscall_type = Exit; break;
    case 5: appel_syst.syscall_type = Wait; break;
    default: appel_syst.syscall_type = Invalid; break;
    };
    return appel_syst;
};


void fork2(Kernel_state* kernel, int* fork_val) {/*fork_val[0] = priorité à donner au fils,
    reste du tableau = paramètres pour initialiser ses registres.
    fork2 pour éviter des conflits de nom avec une fonction de c*/
    kernel->registers[0] = 0; /*On met 0 dans r0 si l'appel système échoue (priorité invalide, pas de processus libre), 1 sinon.*/
    if (fork_val[0] <= kernel->curr_prio) {/* Dans le code OCaml, la vérification est faite dans la fonction transition.*/
        for (int i = 0; i < NUM_PROCESSES; i++) {
            if (kernel->processes[i].state.process_state_type == Free) {
                kernel->processes[i].state.process_state_type = Runnable;
                kernel->processes[i].parent_id = kernel->curr_pid;
                kernel->processes[i].slices_left = MAX_TIME_SLICES;
                kernel->processes[i].saved_context[0] = 2; /*Je n'ai pas encore compris d'où sort ce 2.*/
                kernel->processes[i].saved_context[1] = kernel->curr_pid;
                for (int j = 1; j < NUM_REGISTERS - 1; j++) {
                    kernel->processes[i].saved_context[j + 1] = fork_val[j];
                };
                kernel->registers[0] = 1;
                kernel->registers[1] = i;
                for (int j = 0;j < NUM_PROCESSES; j++) {/*On insère le fils parmi les processus de priorité nprio.
                    S'il n'y a pas d'erreur par ailleurs, on trouve nécessairement une case libre car le tableau est assez grand. */
                    if (kernel->runqueues[fork_val[0]][j] == -1) { kernel->runqueues[fork_val[0]][j] = i; break; };
                    /* On l'insère dans la première case libre. Attention à ne pas l'insérer plusieurs fois: break.*/
                };
                break; /* A nouveau, attention à ne pas créer plusieurs fils.*/
            };
        };
    };
};


void exit2(Kernel_state* kernel) {/*exit2 pour éviter des problèmes avec la fonction exit de c.*/
    /* 1- Les fils du processus qui termine vont avoir pour père 1*/
    for (int i = 0; i < NUM_PROCESSES; i++) {
        if (kernel->processes[i].parent_id == kernel->curr_pid) {
            kernel->processes[i].parent_id = 1;
        };
    };
    /* 2- On élimine le processus qui termine de "runqueue".*/
    /* runqueues est un tableau de listes simulées par des tableaux.
    Ici, pour retirer un processus on se contente de remplacer son id par -1, sans rien décaler.
    Cela signifie que les cases réellement utilisées du tableau ne sont pas forcément les premières,
    il peut y avoir des "trous", et il faut parcourir tout le tableau à chaque fois. */
    for (int i = 0; i < NUM_PROCESSES; i++) {
        if (kernel->runqueues[kernel->curr_prio][i] == kernel->curr_pid) {
            kernel->runqueues[kernel->curr_prio][i] = -1;
        }; /* Cette condition est vérifiée exactement une fois si tout est correct par ailleurs.*/
    };
    /* 3- Si le père attend, l'appel réussit immédiatement, sinon le processus qui termine devient Zombie*/
    Process p = kernel->processes[kernel->curr_pid]; /*processus qui termine */
    Process pp = kernel->processes[p.parent_id]; /* son père*/
    if (pp.state.process_state_type == Waiting) {
        kernel->processes[kernel->curr_pid].state.process_state_type = Free;
        /* Attention, ne pas écrire p.state.process_state_type = Free, qui ne modifie pas kernel.processes[curr_pid]*/
        kernel->processes[p.parent_id].state.process_state_type = Runnable;
        /* On remplit les registres du père :*/
        kernel->processes[p.parent_id].saved_context[0] = 1;
        kernel->processes[p.parent_id].saved_context[1] = kernel->curr_pid;
        kernel->processes[p.parent_id].saved_context[2] = kernel->registers[0];
    }
    else {
        kernel->processes[kernel->curr_pid].state.process_state_type = Zombie;
    };
};

/* A chaque fois qu'on renvoie entier dans les fonctions qui suivent, c'est 1 s'il faut réordonnancer après et 0 sinon.
L'entier renvoyé est aussi le contenu de r0.*/

int wait(Kernel_state* kernel) {
    /* On commence par déterminer si le processus qui attend a des fils, et si l'un d'entre eux est zombie.*/
    /* Je crois qu'il y a une différence entre ce code et le code OCaml.(Ici, on place 1 dans le registre quand le
    processus continue à attendre, dans le code OCaml on ne modifie pas le registre dans ce cas. Je pense que ça ne change rien.*/
    int resultat = 0;
    kernel->registers[0] = 0; 
    int a_un_fils = 0;
    int id_fils_zombie = -1;
    for (int i = 0; i < NUM_PROCESSES; i++) {
        if (kernel->processes[i].parent_id == kernel->curr_pid) {
            a_un_fils = 1;
            if (kernel->processes[i].state.process_state_type == Zombie) {
                id_fils_zombie = i;
                break; /* pas besoin de chercher plus loin */
            };
        };
    };
    if (a_un_fils == 1) {
        if (id_fils_zombie == -1) {
            kernel->processes[kernel->curr_pid].state.process_state_type = Waiting;
        }
        else {
            kernel->processes[id_fils_zombie].state.process_state_type = Free;
            kernel->processes[kernel->curr_pid].saved_context[0] = 1;
            kernel->processes[kernel->curr_pid].saved_context[1] = id_fils_zombie;
            kernel->processes[kernel->curr_pid].saved_context[2] = kernel->processes[id_fils_zombie].saved_context[0];
        };
        kernel->registers[0] = 1;
        resultat = 1;
    };
    return resultat;
};

void new_channel(Kernel_state* kernel) { 
    /* Dans le code OCaml, renvoie l'indice du canal créé, -1 si aucun.
    Mais cela contredit la suite du code ! cf transition.
    A la place, j'écris cette valeur dans r0.*/
    kernel->registers[0] = -1;
    for (int i = 0; i < NUM_CHANNELS; i++) {
        if (kernel->channels[i].channel_state_type == Unused) {
            kernel->channels[i].channel_state_type = Receivers;
            for (int j = 0; j < NUM_PROCESSES; j++) {/* On met des -1 partout pour simuler une liste vide.*/
                kernel->channels[i].receivers_pid_prio[j][0] = -1;
                kernel->channels[i].receivers_pid_prio[j][1] = -1;
            };
            kernel->registers[0] = i;
            break; /*On s'arrête, un seul canal suffit! */
        };
    };
};


int find_max_prio(int tab[][2], int taille_tab) {
    /* Fonction auxiliaire pour send. tab est une matrice taille_tab*2.
    Renvoie j un indice tel que pour tous i tab[i][1] <= tab[j][1].
    Normalement, on aura toujours taille_tab = NUM_PROCESSES*/
    int j = 0;
    for (int i = 0; i < taille_tab; i++) {
        if (tab[i][1] > tab[j][1]) { j = i; };
    };
    return j;
};

int send(Kernel_state *kernel, int cid, int  v) {
    /* cid = indice du canal, v = valeur à envoyer.
    La valeur de retour est la même que celle inscrite dans r0, 1 si l'appel a réussi et 0 sinon.*/
    int resultat = 0;
    if (0 <= cid && cid < NUM_CHANNELS) {
        switch (kernel->channels[cid].channel_state_type) {
        case Unused: kernel->registers[0] = 0; break;
        case Sender: kernel->registers[0] = 0; break;
        default: ; /* J'ai été obligé de mettre ces points virgules inutiles, sinon j'avais une erreur
                   "error: a label can only be part of a statement and a declaration is not a statement"*/
            int rid = find_max_prio(kernel->channels[cid].receivers_pid_prio, NUM_PROCESSES);
            if (rid == -1) /*aucun processus n'écoute*/ {
                kernel->channels[cid].channel_state_type = Sender;
                kernel->channels[cid].sender_pid_priority_value[0] = kernel->curr_pid;
                kernel->channels[cid].sender_pid_priority_value[1] = kernel->curr_prio;
                kernel->channels[cid].sender_pid_priority_value[2] = v;
            }
            else {
                kernel->processes[rid].state.process_state_type = Runnable; /* On débloque le processus qui reçoit (rid).*/
                kernel->channels[cid].receivers_pid_prio[rid][0] = -1; /* On retire le processus rid du canal.*/
                kernel->channels[cid].receivers_pid_prio[rid][1] = -1;
                kernel->processes[rid].saved_context[0] = 1; /* On écrit ce qu'il faut dans les registres de rid*/
                kernel->processes[rid].saved_context[1] = cid;
                kernel->processes[rid].saved_context[2] = v;
                kernel->registers[0] = 1; /* On renvoie vrai.*/
                resultat = 1;
            };
        };
    }
    else { kernel->registers[0] = 0; }; /* Le cas où le numéro du canal est invalide.*/
    return resultat;
};

int receive(Kernel_state* kernel, int cids[NUM_REGISTERS - 1]) {
    /* Les arguments cids sont des entiers qui représentent un canal dans lequel on veut écouter, ou -1.*/
/* On commence par déterminer 2 choses:
- si l'un des cid est un canal dans l'état Sender (auquel cas l'appel réussit tout de suite)
- si au moins l'un des cid est un canal valide (:= entre 0 et NUM_CHANNELS - 1 et Sender ou Receivers*/
/* On ne peut écouter que dans NUM_REGISTERS - 1 canaux à la fois car il faut noter dans les registres du récepteur
tous les canaux dans lequel il écoute, et on n'utilise pas r0 pour ça.*/
/* Il est écrit dans la spécification que la fonction ne renvoie rien, mais il renvoie la valeur écrite dans r0
dans son code OCaml, donc je fais pareil.*/
    int resultat = 0;
    int ch_sender = -1; /*  -1 si aucun des entiers dans cids n'est l'identifiant d'un canal dans l'état Sender,
                    sinon l'indice d'un d'entre eux*/
    int existe_canal_valide = 0;
    for (int i = 0; i < NUM_REGISTERS - 1; i++) {
        if ((0 <= cids[i]) && (cids[i] < NUM_CHANNELS) && (kernel->channels[cids[i]].channel_state_type != Unused)) {
            existe_canal_valide = 1;
            if (kernel->channels[cids[i]].channel_state_type == Sender) {
                ch_sender = cids[i];
                break;
            };
        };
    };
    if (!existe_canal_valide) { kernel->registers[0] = 0; }
    else {
        if (ch_sender != -1) {
            int sid = kernel->channels[ch_sender].sender_pid_priority_value[0]; /*On récupère les info sur l'émetteur.*/
            int sprio = kernel->channels[ch_sender].sender_pid_priority_value[1];
            int sval = kernel->channels[ch_sender].sender_pid_priority_value[2];
            kernel->channels[ch_sender].channel_state_type = Receivers; /* On libère le canal (plus personne n'y émet).*/
            for (int i = 0; i < NUM_PROCESSES; i++) {
                kernel->channels[ch_sender].receivers_pid_prio[i][0] = -1;
                kernel->channels[ch_sender].receivers_pid_prio[i][1] = -1;
            };
            kernel->processes[sid].state.process_state_type = Runnable;
            kernel->registers[0] = 1;
            kernel->registers[1] = ch_sender;
            kernel->registers[2] = sval;
            if (sprio >= kernel->curr_prio) { resultat = 1; }; /* Je ne comprends pas pourquoi on fait ça...*/
        }
        else {
            kernel->processes[kernel->curr_pid].state.process_state_type = BlockedReading;
            for (int i = 0; i < NUM_REGISTERS - 1; i++) {
                if ((0 <= cids[i]) && (cids[i] < NUM_CHANNELS) && (kernel->channels[cids[i]].channel_state_type = Receivers)) {
                    kernel->processes[kernel->curr_pid].state.chanidlist[i] = cids[i];
                    for (int j = 0; j < NUM_PROCESSES; j++) {
                        /*On ajoute le processus courant à la liste des processus qui attendent sur le canal*/
                        if (kernel->channels[cids[i]].receivers_pid_prio[j][0] == -1) {
                            kernel->channels[cids[i]].receivers_pid_prio[j][0] = kernel->curr_pid;
                            kernel->channels[cids[i]].receivers_pid_prio[j][0] = kernel->curr_prio;
                            break;
                        };
                    };
                }
                else { kernel->processes[kernel->curr_pid].state.chanidlist[i] = -1; };
            };
        };

    };
    return resultat;
};

void init() {
    kernel.curr_pid = 1;
    kernel.position = 0;
    kernel.count = 0;
    kernel.curr_prio = MAX_PRIORITY; /* Dans le code OCaml, curr_pid = 0 et curr_prio = MAX_PRIORITY, ce qui contredit la spécification.*/
    for (int i = 0; i < NUM_CHANNELS; i++) { kernel.channels[i].channel_state_type = Unused; };
    kernel.runqueues[0][0] = 0; /*Le processus 0, idle, a priorité 0.*/
    kernel.runqueues[MAX_PRIORITY][0] = 1; /* Le processus 1, init, a priorité maximale.*/
    for (int j = 1; j < NUM_PROCESSES; j++) { kernel.runqueues[0][j] = -1; kernel.runqueues[MAX_PRIORITY][j] = -1; };
    for (int i = 1; i < MAX_PRIORITY; i++) {
        for (int j = 0; j < NUM_PROCESSES; j++) { kernel.runqueues[i][j] = -1; };
    };
    for (int i = 0; i < NUM_CHANNELS; i++) { kernel.channels[i].channel_state_type = Unused; };
    for (int i = 0; i < NUM_REGISTERS; i++) { kernel.registers[i] = 0; };
    kernel.processes[0].parent_id = 0;
    kernel.processes[0].state.process_state_type = Runnable;
    kernel.processes[0].slices_left = MAX_TIME_SLICES;
    for (int i = 0; i < NUM_REGISTERS; i++) { kernel.processes[0].saved_context[i] = 0; };
    kernel.processes[1].parent_id = 1;
    kernel.processes[1].state.process_state_type = Wait;
    kernel.processes[1].slices_left = MAX_TIME_SLICES;
    for (int i = 0; i < NUM_REGISTERS; i++) { kernel.processes[1].saved_context[i] = 0; };
    for (int i = 2; i < NUM_PROCESSES; i++) {/*Initialisation des autres processus*/
        kernel.processes[0].parent_id = 0;
        for (int k = 0; k < NUM_REGISTERS; k++) { kernel.processes[i].saved_context[k] = 0; };
        kernel.processes[i].state.process_state_type = Free;
        kernel.processes[i].slices_left = MAX_TIME_SLICES;
    };
};

void save_context(Kernel_state* kernel) {
    for (int i = 0; i < NUM_REGISTERS; i++) {
        kernel->processes[kernel->curr_pid].saved_context[i] = kernel->registers[i];
    };
};

void restore_context(Kernel_state* kernel) {
    for (int i = 0; i < NUM_REGISTERS; i++) {
        kernel->registers[i] = kernel->processes[kernel->curr_pid].saved_context[i];
    };
};

void choose_processus(Kernel_state* kernel) {
    /* choose_processus modifie curr_pid, curr_prio et position de kernel. */
    /* On ne modifie pas count car il ne sert à rien dans cet algorithme. */
    /* Normalement, on trouve toujours un processus car les processus 0 et 1 ne terminent jamais.*/
    int deja_trouve = 0; /*Booléen qui indique si on a déjà trouvé un processus.*/
    for (int prio = MAX_PRIORITY; (prio >= 0) && (deja_trouve = 0); prio--) {
        /* On cherche s'il y a un processus de priorité prio.
        On cherche de position + 1 à la fin, puis du début à position,pour être sûr de bien
        prendre les processus d'égale priorité à tour de rôle, sans favoriser le début du tableau.*/
        for (int i = kernel->position; i < NUM_PROCESSES; i++) {
            if ((kernel->runqueues[prio][i] != -1) && (kernel->runqueues[prio][i] != kernel->curr_pid)) {
            kernel->curr_pid = kernel->runqueues[prio][i];
            kernel->curr_prio = prio;
            kernel->position = i;
            deja_trouve = 1;
            break;
            };
        };
        for (int i = 0; (i < kernel->position) && (deja_trouve = 0); i++) {
            if (kernel->runqueues[prio][i] != -1) {
                kernel->curr_pid = kernel->runqueues[prio][i];
                kernel->curr_prio = prio;
                kernel->position = i;
                deja_trouve = 1;
                break;
            };
        };
    };
};

void choose_processus2(Kernel_state* kernel) {/* Version où un processus a le droit à autant de tours
                                              à la suite que sa priorité.*/
    if (kernel->count >= kernel->curr_prio) {/* Changement de processus*/
        int deja_trouve = 0;
        kernel->count = 0;
        for (int i = kernel->position + 1; i < NUM_PROCESSES; i++) {/*On cherche parmi les processus suivant
                                                                    de même priorité.*/
            kernel->position = i;
            deja_trouve = 1;
            break;
        };
        if (kernel->curr_prio > 0) {/* On cherche parmi les processus de priorité plus faible.*/
            for (int prio = kernel->curr_prio - 1; (prio >= 0) && (deja_trouve = 0); prio--) {
                for (int i = 0; (i < NUM_PROCESSES) && (deja_trouve = 0); i++) {
                    if (kernel->runqueues[prio][i] != -1) {
                        kernel->curr_pid = kernel->runqueues[prio][i];
                        kernel->curr_prio = prio;
                        kernel->position = i;
                        deja_trouve = 1;
                        break;
                    };
                };
            };
        };
        for (int prio = 0; (prio < MAX_PRIORITY) && (deja_trouve = 0); prio++) {
            /*On cherche parmi les processus de prio plus forte.*/
            for (int i = 0; (i < NUM_PROCESSES) && (deja_trouve = 0); i++) {
                if (kernel->runqueues[prio][i] != -1) {
                    kernel->curr_pid = kernel->runqueues[prio][i];
                    kernel->curr_prio = prio;
                    kernel->position = i;
                    deja_trouve = 1;
                    break;
                };
            };
        };
    }
    else {/*Pas de changement*/
        kernel->count = kernel->count + 1;
    };
};

void schedule(Kernel_state* kernel) {
    save_context(kernel);
    choose_processus(kernel);
    restore_context(kernel);
};

int timer_tick(Kernel_state* kernel) {
    int resultat = 0;
    kernel->processes[kernel->curr_pid].slices_left = kernel->processes[kernel->curr_pid].slices_left - 1;
    if (kernel->processes[kernel->curr_pid].slices_left == 0) {
        kernel->processes[kernel->curr_pid].slices_left = MAX_TIME_SLICES; 
        resultat = 1;
    };
    return resultat;
};

void transition(Event ev, Kernel_state* kernel) {
    int reschedule = 0; /* Booléen qui dit si on doit faire appel à schedule (=passer au processus suivant) ou non*/
    switch (ev) {
    case Timer:; reschedule = timer_tick(kernel); break;
    default:; /* = case Syscall*/
        Syscall appel_syst = decode(*kernel);
        switch (appel_syst.syscall_type) {
        case Send:; reschedule = send(kernel, appel_syst.send_val[0], appel_syst.send_val[1]); break;
        case Receive:; reschedule = receive(kernel, appel_syst.receive_val); break;
        case Fork:; fork2(kernel, appel_syst.fork_val); break; /*jamais besoin de réordonnancer*/
        case Wait:; reschedule = wait(kernel); break;
        case Exit:; exit2(kernel); reschedule = 1; break; /*toujours réordonnancement*/
        case NewChannel:; new_channel(kernel); break; /*pas de réordonnancement*/
        default:; kernel->registers[0] = -1; break; /* = case Invalid*/
        };
        break;
    };
    if (reschedule) { schedule(kernel); };
};

