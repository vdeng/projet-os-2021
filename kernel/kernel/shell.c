#include <stddef.h>
#include <keyboard.h>
#include <tty.h>
#include <stdio.h>

#define TAILLE_DOSSIER 10
#define TAILLE_NOM 21 /*20 = taille des chaines de caracteres utilisees pour les noms de fichier
                        Le 21 permet de s'assurer de la presence de '\0' a la fin de la chaine de characteres.*/
#define TAILLE_FICHIER 101 /*Cette taille doit-etre plus grande que 6 et plus grande que TAILLE_NOM pour ne pas poser de problemes
                        dans la fonction lire qui utilise espace_de_lecture[TAILLE_FICHIER]*/
#define TAILLE_SYSTEME 100 /*Indique le nombre total de fichiers/dossiers qui peuvent se trouver dans le systeme de fichiers */

enum type_fichier { Fichier, Dossier, Error };
typedef struct fichier_s {
    enum type_fichier type;
    char nom[TAILLE_NOM];
    struct fichier_s* pere;
    union {
        struct fichier_s *sous_dossier[TAILLE_DOSSIER];
        char contenu[TAILLE_FICHIER];
    };
} fichier ;

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;
static const size_t CMD_MAX_LENGTH = VGA_WIDTH*(VGA_HEIGHT-1);

extern void test_syscall();
void entree_clavier(char entree[]);

void shell() {
    char command_buffer[CMD_MAX_LENGTH+1];
    while(true) {
        size_t command_length = 0;
        printf("MyOS > ");
        while(true) {
            struct kbd_event ev = kbd_poll_event();
            bool ctrl = ev.lctrl || ev.rctrl, alt = ev.lalt || ev.ralt;
            if(ev.pressed) {
                if(ev.keycode == KBD_BACKSPACE) {
                    if(command_length > 0) {
                        command_length--;
                        size_t row = terminal_get_row();
                        size_t column = terminal_get_column();
                        if(column > 0) {
                            terminal_set_position(row, column-1);
                        }
                        else {
                            terminal_set_position(row-1, VGA_WIDTH-1);
                        }
                        terminal_erase_curr();
                    }
                }
                else if(ev.keycode == KBD_ENTER) {
                    printf("\n");
                    break;
                }
                else if(ev.charcode != '\0' && command_length < CMD_MAX_LENGTH && !ctrl && !alt) {
                    printf("%c", ev.charcode);
                    command_buffer[command_length++] = ev.charcode;
                }
            }
        }
        command_buffer[command_length] = '\0';
        /*printf("Your command was: %s\nIt will be processed in a future version of MyOS. Be patient!\n", command_buffer);*/
        entree_clavier(command_buffer);
        test_syscall();
    }
}


fichier home = {.type = Dossier, .nom="home"};
/* home est une variable globale representant le dossier racine, qui est indispensable.*/
fichier* curr_dir = &home;
/* curr_dir est une variable globale, un pointeur vers le dossier courant. Au debut, c'est home.*/
fichier error = {.type = Error};
/* ce fichier permet aux fonctions de type fichier* de renvoyer quelque chose qui symbolise l'interruption dûûe a une erreur */
char error_chain[1];
/* de meme, cette chaine represente les erreurs des fonctions qui renvoient des chaines de caracteres */
fichier systeme_de_fichiers[TAILLE_SYSTEME];
/* ce tableau represente l'espace memoire dans lequel les dossiers et fichiers sont enregistres. En tant que variable globale, il permet la 'creation' de nouveaux dossiers et fichiers en attribuant les dossiers et fichiers qu'il contient. */
int nb_df = 0;
/* cette variable gloable enregistre le nombre de dossier et fichiers existants, et permet de savoir jusqu'où systeme_de_fichier est rempli par des dossiers et fichiers pertinents. */
char espace_de_lecture[TAILLE_FICHIER];
/* cette variable globale servt a attribuer de l'espace memoire pour stocker une chaine de caractere, ce qui est utile pour la fonction lire qui ne peut attribuer de l'espace memoire elle-meme dans notre OS */

int longueur(char chaine[]){
    int n = 0;
    char caractereActuel = 0;
    do
    {
        caractereActuel = chaine[n];
        n++;    
    }
    while(caractereActuel != '\0'); // On boucle tant qu'on n'est pas arrive a la fin de la chaine
    n--;
    return n;
};

int sont_egales(char chaine1[],char chaine2[]){
    int i = 0;
    while (chaine1[i] == chaine2[i] && chaine1[i] != '\0' && chaine2[i] != '\0'){
        i += 1;
    };
    if (!(chaine1[i] == '\0' && chaine2[i] == '\0')){
        return 0;
    };
    return 1;
};

int prochaine_position(char chaine[],int d,char c){ //renvoie la prochaine position de c dans chaine apres la position d
    int f = d;
    char caractereActuel = 0;
    do
    {
        caractereActuel = chaine[f];
        f++;    
    }
    while(caractereActuel != '\0' && caractereActuel != c); // On boucle tant qu'on n'est pas arrive a la fin de la chaine ou qu'on n'a pas trouve c
    if (caractereActuel == '\0'){
        return -1;
    };
    f--;
    return f;
};

char *lire(char chaine[],int d,int f){ /*L'element en place f n'est pas pris en compte*/
    if (f-d >= TAILLE_FICHIER){
        printf("Erreur: une chaine de caracteres aussi longue n'est pas acceptee.\n");
        return error_chain;
    };
    for (int i = 0; i < f-d ;i++){
        espace_de_lecture[i] = chaine[d+i];
    };
    espace_de_lecture[f-d] = '\0';
    return(espace_de_lecture);
};

fichier* search_child(fichier* dir, char fils[]){
/* Une fonction qui prend un pointeur vers un dossier dir et un nom de fils et renvoie, s'il le trouve, un
pointeur vers un fichier fils de dir ayant le nom requis. Cette fonction servira pour la fonction auxiliaire
suivante. Il faut s'arranger pour que le message d'erreur souhaite soit affiche si on l'appelle depuis 
follow_path.*/
    if (longueur(fils)==0){
        printf("Erreur: le chemin est invalide\n");
        return &error;
    };
    fichier f_curr = *dir;
    if (sont_egales(fils,"..")){
        if (f_curr.type == Fichier){
            printf("Erreur: %s n'est pas un Dossier.\n",f_curr.nom);
            return &error;
        };
        if (dir == &home){
            printf("Erreur: le dossier racine home n'a pas de dossier parent.\n");
            return &error;
        };
        return f_curr.pere;
    };
    for (int i = 0 ; i < TAILLE_DOSSIER ; i++)
        {
            if (f_curr.sous_dossier[i] != 0) {
                fichier f = *f_curr.sous_dossier[i] ;
                if (sont_egales(f.nom,fils)){
                    return f_curr.sous_dossier[i];
                };
            };
        }
    printf("Erreur: Le dossier %s ne contient pas de fichier ou dossier %s.\n",f_curr.nom,fils);
    return &error;
};

fichier* follow_path(char chemin[]){
/* Une fonction qui servira pour plusieurs des commandes ci-dessous. Elle prend une chaine de caracteres 
censee representer un chemin et renvoie un pointeur vers le fichier (fichier texte ou dossier) destination
du chemin. Pour ce faire, elle lit la chaine jusqu'a tomber sur un slash 
Sinon, elle renvoie un message d'erreur. Ce message d'erreur precise le premier nom de fichier/
dossier pas trouve.*/
    int n = longueur(chemin);
    int d = 0;
    int d2 = 0;
    fichier* f = curr_dir;
    char *fils = NULL;
    if (n-d==0){
        printf("Le chemin vide n'est pas un chemin valide.\n");
        return &error;
    };
    while(n-d>0){
        d2 = prochaine_position(chemin,d,'/');
        if (d2 == -1){
            fils = chemin + d;
            d = n;
        }
        else {
            fils = lire(chemin,d,d2);
            if (fils==error_chain){
                return &error;
            };
            d = d2 + 1;
        };
        f = search_child(f,fils);
        if (f==&error){
            return &error;
        };
    };
    return f;
};

void echo(char chaine[]){
/* chaine peut etre un chemin vers le fichier dont l'utilisateur veut afficher le contenu, ou une chaine de
caractere (si l'utilisateur a saisi "echo "Coucou"", chaine est une chaine de caractere dont le premier 
caractere est: " ). Il y a donc 2 cas a traiter.*/
    int n = longueur(chaine);
    if (chaine[0] == '"'){
        if (chaine[n-1] != '"' || (n-1) == 0){
            printf("Erreur: Argument invalide. echo prend comme argument soit un chemin vers un fichier, soit une chaine de caracteres.\n");
            return;
        };
        char *a_afficher = NULL;
        a_afficher = lire(chaine,1,n-1);
        if (a_afficher==error_chain){
            return;
        };
        printf("%s\n",a_afficher);
        return;
    };
    fichier* f;
    f = follow_path(chaine);
    if (f==&error){
        return;
    };
    if ((*f).type != Fichier){
        printf("Erreur: echo n'affiche pas le contenu d'un dossier, mais seulement d'un fichier.\n");
        return;
    };
    printf("%s\n",(*f).contenu);
    return;
};

void ls(char chemin[]) {
/* ls prend soit un chemin reel comme argument, soit aucun argument - chemin est la chaine de caracteres vide dans ce cas la*/
    fichier* f = curr_dir;
    int n = longueur(chemin);
    if (n!=0){
        f = follow_path(chemin);
    };
    if (f==&error){
        return;
    };
    fichier f_curr = *f;
    if (f_curr.type == Fichier){
        printf("Erreur: %s n'est pas un dossier.\n",f_curr.nom);
        return;
    };
    printf("Dossiers :\n");
    for (int i = 0 ; i < TAILLE_DOSSIER ; i++)
        {
            if (f_curr.sous_dossier[i] != 0) {
                fichier f = *f_curr.sous_dossier[i] ;
                if (f.type == Dossier){
                    printf("%s ",f.nom);
                };
            };
        }
    printf("\n");
    printf("Fichiers :\n");
    for (int i = 0 ; i < TAILLE_DOSSIER ; i++)
        {
            if (f_curr.sous_dossier[i] != 0) {
                fichier f = *f_curr.sous_dossier[i] ;
                if (f.type == Fichier){
                    printf("%s ",f.nom);
                };
            };
        }
    printf("\n");
}; /* On pourrait ameliorer cette fonction en rajoutant un tri par ordre alphabetique*/

void path(fichier* f) {
/* Fonction auxiliaire cree pour faciliter le retour a la ligne apres pwd */
    fichier fi = *f;
    if (f != &home){
        path(fi.pere);
    };
    printf("/%s",fi.nom);
};

void pwd() {
/* On suppose qu'on a deja verifie que la chaine de caracteres etait "pwd" exactement.
Cette verification est faite de sorte a s'assurer que l'utilisateur/utilisatrice commprenne
que pwd ne prend pas d'argument et indique seulement le chemin du dossier actuel.*/
    path(curr_dir);
    printf("\n");
};

void cd(char chemin[]){
    fichier* f = follow_path(chemin);
    if (f==&error){
        return;
    };
    fichier f_curr = *f;
    if (f_curr.type == Fichier){
        printf("Erreur: %s n'est pas un dossier.\n",f_curr.nom);
        return;
    };
    curr_dir = f;
    return;
};

int nom_valide(fichier f,char nom[]){
/*Un nom valide est un nom nom vide qui n'est pas trop long, n'a ni espace, ni slash, ni guillemets, et ne corresponde pas deja a un
nom de dossier ou de fichier contenu dans le fichier f.*/
    if (longueur(nom) > TAILLE_NOM-1){
        printf("Erreur: %s est un nom trop long. Un nom de fichier ou de dossier doit etre d'au plus %d caracteres.\n",nom,TAILLE_NOM-1);
        return 0;
    };
    if (longueur(nom) == 0){
        printf("Erreur: Veuillez donner un nom au fichier ou dossier que vous souhaitez creer.\n");
        return 0;
    };
    if (prochaine_position(nom,0,' ') != -1){
        printf("Erreur: Un nom de fichier ou de dossier ne peut pas contenir d'espace.\n");
        return 0;
    };
    if (prochaine_position(nom,0,'/') != -1){
        printf("Erreur: Un nom de fichier ou de dossier ne peut pas contenir de slash '/'.\n");
        return 0;
    };
    if (prochaine_position(nom,0,'"') != -1){
        printf("Erreur: Un nom de fichier ou de dossier ne peut pas contenir de guillemets ' \" '.\n");
        return 0;
    };
    for (int i = 0 ; i < TAILLE_DOSSIER ; i++){
        if (f.sous_dossier[i] != 0) {
            fichier fils = *f.sous_dossier[i] ;
            if (sont_egales(fils.nom,nom)){
                printf("Erreur: Il y a deja un fichier ou un dossier de nom %s dans %s.\n",nom,f.nom);
                return 0;
            };
        };
    };
    return 1;
};

int emplacement_libre(fichier f){
/* aide a trouver un emplacement libre dans le tableau contenu du dossier */
    for (int i = 0 ; i < TAILLE_DOSSIER ; i++){
        if (f.sous_dossier[i] == 0) {
            return i;
        };
    };
    return -1;
}

void touch(char chemin[]){
/* Cree un fichier vide dont le chemin a partir du dossier courant sera celui passe en parametre.
Seul le fichier peut-etre cree: les dossiers intermediaires du chemin doivent deja exister.*/
//    int n = longueur(chemin);
    int d = 0;
    int d2 = 0;
    fichier* f = curr_dir;
    char *fils = NULL;
    d2 = prochaine_position(chemin,d,'/');
    while(d2 != -1){
        fils = lire(chemin,d,d2);
        if (fils==error_chain){
            return;
        };
        d = d2 + 1;
        f = search_child(f,fils);
        if (f==&error){
            return;
        };
        d2 = prochaine_position(chemin,d,'/');
    };
    char *nom = chemin;
    nom = chemin + d;

    if ((*f).type == Fichier){
        printf("Erreur: %s est un fichier, on ne peut pas creer de fichier a l'interieur d'un fichier.\n",(*f).nom);
        return;
    };
    

    if (!(nom_valide((*f),nom))){
        return;
    };
    int r = emplacement_libre((*f));
    if (r == -1){
        printf("Erreur: Le dossier %s est deja plein.\n",(*f).nom);
        return;
    };
    if (nb_df + 1 == TAILLE_SYSTEME){
        printf("Erreur: le systeme de fichiers est plein.\n");
        return;
    };
    systeme_de_fichiers[nb_df].type = Fichier;
    systeme_de_fichiers[nb_df].pere = f;
    systeme_de_fichiers[nb_df].contenu[0] = '\0';
    int i = 0;
    while (nom[i] != '\0'){
        systeme_de_fichiers[nb_df].nom[i] = nom[i];
        i++;
    };
    (*f).sous_dossier[r] = &systeme_de_fichiers[nb_df];
    nb_df++;
};

void mkdir(char chemin[]){
/* Idem mais pour un dossier.*/
//    int n = longueur(chemin);
    int d = 0;
    int d2 = 0;
    fichier* f = curr_dir;
    char *fils = NULL;
    d2 = prochaine_position(chemin,d,'/');
    while(d2 != -1){
        fils = lire(chemin,d,d2);
        if (fils==error_chain){
            return;
        };
        d = d2 + 1;
        f = search_child(f,fils);
        if (f==&error){
            return;
        };
        d2 = prochaine_position(chemin,d,'/');
    };
    char *nom = chemin;
    nom = chemin + d;

    if ((*f).type == Fichier){
        printf("Erreur: %s est un fichier, on ne peut pas creer de dossier a l'interieur d'un fichier.\n",(*f).nom);
        return;
    };
    

    if (!(nom_valide((*f),nom))){
        return;
    };
    int r = emplacement_libre((*f));
    if (r == -1){
        printf("Erreur: Le dossier %s est deja plein.\n",(*f).nom);
        return;
    };
    if (nb_df + 1 == TAILLE_SYSTEME){
        printf("Erreur: le systeme de fichiers est plein.\n");
        return;
    };
    systeme_de_fichiers[nb_df].type = Dossier;
    systeme_de_fichiers[nb_df].pere = f;
    for (int i = 0; i < TAILLE_DOSSIER; i++){
        systeme_de_fichiers[nb_df].sous_dossier[i] = NULL;
    };
    int i = 0;
    while (nom[i] != '\0'){
        systeme_de_fichiers[nb_df].nom[i] = nom[i];
        i++;
    };
    (*f).sous_dossier[r] = &systeme_de_fichiers[nb_df];
    nb_df++;
};

int dossier_non_vide(fichier* f){
    if ((*f).type == Fichier){
        return -1;
    }
    for (int i = 0; i < TAILLE_DOSSIER; i++){
        if ((*f).sous_dossier[i] != 0){
            return i;
        };
    }
    return -1;
};

void rm(fichier* f){
    int r = dossier_non_vide(f);
    if (r != -1){
        rm((*f).sous_dossier[r]);
        rm(f);
        return;
    };
    int i = 0;
    while (i<nb_df && &(systeme_de_fichiers[i]) != f ){
        i++;
    };
    if (i==nb_df){
        printf("On ne peut pas effacer le dossier racine.\n");
        return;
    };
    fichier* pere = (*f).pere;
    for (int i = 0; i < TAILLE_DOSSIER; i++){
        if ((*pere).sous_dossier[i] == f){
            (*pere).sous_dossier[i] = 0;
        };
    }
    systeme_de_fichiers[i] = systeme_de_fichiers[nb_df-1];
    nb_df--;
};

void rm_fichier(char chemin[]){
    fichier* f = curr_dir;
    f = follow_path(chemin);
    if (f==&error){
        return;
    };
    if ((*f).type == Dossier){
        printf("Erreur: %s est un dossier. Si vous voulez vraiment le supprimer, faites rm -d %s.\n",(*f).nom,chemin);
        return;
    };
    rm(f);
};

void rm_r(char chemin[]){
/* rm fifi detruit le (vrai) fichier nomme fifi s'il en existe un dans le dossier courant et 
renvoie un message d'erreur sinon. rm didi, où didi est un dossier, renvoie un message d'erreur disant qu'on
ne peut supprimer un dossier. rm -r didi où didi est un dossier et rm -r fifi marchent.*/
    fichier* f = curr_dir;
    f = follow_path(chemin);
    if (f==&error){
        return;
    };
    rm(f);
};

void cp(fichier* fi, fichier* da){
    fichier f = home;
    f = *fi;
    if (f.type == Fichier){
        if (!(nom_valide((*da),f.nom))){
            return;
        };
        int r = emplacement_libre((*da));
        if (r == -1){
            printf("Erreur: Le dossier %s est deja plein.\n",(*da).nom);
            return;
        };
        if (nb_df + 1 == TAILLE_SYSTEME){
            printf("Erreur: le systeme de fichiers est plein.\n");
            return;
        };
        systeme_de_fichiers[nb_df].type = Fichier;
        systeme_de_fichiers[nb_df].pere = da;
        int k = 0;
        while(f.contenu[k] != '\0'){
            systeme_de_fichiers[nb_df].contenu[k] = f.contenu[k];
            k++;
        };
        systeme_de_fichiers[nb_df].contenu[k] = '\0';
        int i = 0;
        while (f.nom[i] != '\0'){
            systeme_de_fichiers[nb_df].nom[i] = f.nom[i];
            i++;
        };
        (*da).sous_dossier[r] = &systeme_de_fichiers[nb_df];
        nb_df++;
        return;
    };

    if (!(nom_valide((*da),f.nom))){
        return;
    };
    int r = emplacement_libre((*da));
    if (r == -1){
        printf("Erreur: Le dossier %s est deja plein.\n",(*da).nom);
        return;
    };
    if (nb_df + 1 == TAILLE_SYSTEME){
        printf("Erreur: le systeme de fichiers est plein.\n");
        return;
    };
    systeme_de_fichiers[nb_df].type = Dossier;
    systeme_de_fichiers[nb_df].pere = da;
    systeme_de_fichiers[nb_df].sous_dossier[0] = NULL;
    int i = 0;
    while (f.nom[i] != '\0'){
        systeme_de_fichiers[nb_df].nom[i] = f.nom[i];
        i++;
    };
    (*da).sous_dossier[r] = &systeme_de_fichiers[nb_df];
    nb_df++;
    for (int h = 0; h< TAILLE_DOSSIER; h++){
        if (f.sous_dossier[h] != 0){
            cp(f.sous_dossier[h],&systeme_de_fichiers[nb_df]);
        };
    };
};

void cp_fichier(char chaine[]){
    int p = prochaine_position(chaine,0,' ');
    if (p == -1){
        printf("Erreur: Commande invalide. Essayez cp chemin-fichier chemin2 pour copier un fichier dans chemin2, et cp -d chemin-fichier chemin2 pour copier un fichier ou dossier dans chemin2. Si le dossier d'arrivee est le dossier courant, chemin2 peut etre remplace par un simple point '.'.\n");
        return;
    };
    char *chemin2 = chaine + p + 1;
    char *chemin1 = chaine;
    chemin1 = lire(chaine,0,p);
    if (chemin1==error_chain){
        return;
    };
    fichier* fi = curr_dir;
    fi = follow_path(chemin1);
    if (fi==&error){
        return;
    };
    if ((*fi).type != Fichier){
        printf("Erreur: Commande invalide. Essayez cp -d chemin-fichier chemin2 pour copier un fichier ou dossier dans chemin2. Si le dossier d'arrivee est le dossier courant, chemin2 peut etre remplace par un simple point '.'.\n");
        return;
    };
    fichier* da = curr_dir;
    if (!(sont_egales(chemin2,"."))){
        da = follow_path(chemin2);
        if (da==&error){
            return;
        };
    };
    if ((*da).type != Dossier){
        printf("Erreur: Commande invalide. %s n'est pas un dossier d'arrivee valide.\n",(*da).nom);
        return;
    };
    cp(fi,da);
};

void cp_d(char chaine[]){
/* cp chemin_fifi chemin_didi copie le fichier dont le chemin est chemin_fifi vers le dossier dont le
chemin est chemin_didi. Erreur si chemin_fifi est un chemin vers un dossier. cp -d chemin_didi1 chemin_didi2
copie didi1 et son contenu dans didi2. Le chemin_did2 peut etre remplace par un point '.' si didi2 est le fichier courant */
    int p = prochaine_position(chaine,0,' ');
    if (p == -1){
        printf("Erreur: Commande invalide. Essayez cp chemin-fichier chemin2 pour copier un fichier dans chemin2, et cp -d chemin-fichier chemin2 pour copier un fichier ou dossier dans chemin2. Si le dossier d'arrivee est le dossier courant, chemin2 peut etre remplace par un simple point '.'.\n");
        return;
    };
    char *chemin2 = chaine + p + 1;
    char *chemin1 = chaine;
    chemin1 = lire(chaine,0,p);
    if (chemin1==error_chain){
        return;
    };
    fichier* fi = curr_dir;
    fi = follow_path(chemin1);
    if (fi==&error){
        return;
    };
    fichier* da = curr_dir;
    if (!(sont_egales(chemin2,"."))){
        da = follow_path(chemin2);
        if (da==&error){
            return;
        };
    };
    if ((*da).type != Dossier){
        printf("Erreur: Commande invalide. %s n'est pas un dossier d'arrivee valide.\n",(*da).nom);
        return;
    };
    cp(fi,da);
};

void mv_fichier(char chaine[]){
    int p = prochaine_position(chaine,0,' ');
    if (p == -1){
        printf("Erreur: Commande invalide. Essayez mv chemin-fichier chemin2 pour deplacer un fichier dans chemin2, et mv -d chemin-fichier chemin2 pour deplacer un fichier ou dossier dans chemin2. Si le dossier d'arrivee est le dossier courant, chemin2 peut etre remplace par un simple point '.'.\n");
        return;
    };
    char *chemin2 = chaine + p + 1;
    char *chemin1 = chaine;
    chemin1 = lire(chaine,0,p);
    if (chemin1==error_chain){
        return;
    };
    fichier* fi = curr_dir;
    fi = follow_path(chemin1);
    if (fi==&error){
        return;
    };
    if ((*fi).type != Fichier){
        printf("Erreur: Commande invalide. Essayez mv -d chemin-dossier chemin2 pour deplacer un dossier dans chemin2.\n");
        return;
    };
    fichier* da = curr_dir;
    if (!(sont_egales(chemin2,"."))){
        da = follow_path(chemin2);
        if (da==&error){
            return;
        };
    };
    if ((*da).type != Dossier){
        printf("Erreur: Commande invalide. %s n'est pas un dossier d'arrivee valide.\n",(*da).nom);
        return;
    };

    if (!(nom_valide((*da),(*fi).nom))){
        return;
    };
    int r = emplacement_libre((*da));
    if (r == -1){
        printf("Erreur: Le dossier %s est deja plein.\n",(*da).nom);
        return;
    };
    (*da).sous_dossier[r] = fi;

    fichier* pere = curr_dir;
    pere = (*fi).pere;
    (*fi).pere = da;
    for (int i = 0; i < TAILLE_DOSSIER; i++){
        if ( (*pere).sous_dossier[i] == fi ){
            (*pere).sous_dossier[i] = 0;
        };
    };
};

void mv_d(char chaine[]){
    int p = prochaine_position(chaine,0,' ');
    if (p == -1){
        printf("Erreur: Commande invalide. Essayez mv chemin-fichier chemin2 pour deplacer un fichier dans chemin2, et mv -d chemin-fichier chemin2 pour deplacer un fichier ou dossier dans chemin2. Si le dossier d'arrivee est le dossier courant, chemin2 peut etre remplace par un simple point '.'.\n");
        return;
    };
    char *chemin2 = chaine + p + 1;
    char *chemin1 = chaine;
    chemin1 = lire(chaine,0,p);
    if (chemin1==error_chain){
        return;
    };
    fichier* fi = curr_dir;
    fi = follow_path(chemin1);
    if (fi==&error){
        return;
    };
    fichier* da = curr_dir;
    if (!(sont_egales(chemin2,"."))){
        da = follow_path(chemin2);
        if (da==&error){
            return;
        };
    };
    if ((*da).type != Dossier){
        printf("Erreur: Commande invalide. %s n'est pas un dossier d'arrivee valide.\n",(*da).nom);
        return;
    };

    if (!(nom_valide((*da),(*fi).nom))){
        return;
    };
    int r = emplacement_libre((*da));
    if (r == -1){
        printf("Erreur: Le dossier %s est deja plein.\n",(*da).nom);
        return;
    };
    (*da).sous_dossier[r] = fi;

    if (fi == &home){
        printf("Erreur: On ne peut pas deplacer le dossier racine.\n");
        return;
    };
    
    fichier* pere = curr_dir;
    pere = (*fi).pere;
    (*fi).pere = da;
    for (int i = 0; i < TAILLE_DOSSIER; i++){
        if ( (*pere).sous_dossier[i] == fi ){
            (*pere).sous_dossier[i] = 0;
        };
    };
};

/* Ce qu'il manque, c'est des fonctions pour modifier les fichiers. En bash, cela ne se fait pas si
simplement, on utilise des trucs sophistiques comme cat > fifi. Il faut donc inventer des commandes de 
specifiques a notre OS. Je propose les suivantes.*/

void add(char chaine[]){
/*add chemin_fifi "blabla" ajoute blabla a la fin du fichier fifi */
    int p = prochaine_position(chaine,0,' ');
    char *chemin = chaine;
    if (p != -1) {
        chemin = lire(chaine,0,p);
        if (chemin==error_chain){
            return;
        };
    };
    fichier* f = curr_dir;
    f = follow_path(chemin);
    if (f==&error){
        return;
    };
    if ( (*f).type != Fichier ){
        printf("Erreur: %s n'est pas un fichier\n",(*f).nom);
        return;
    };
    int nc = longueur(chaine) - p - 3;
    if (chaine[p+1] != '"' || chaine[nc+p+2] != '"'){
        printf("Erreur: chaine de caracteres invalide, il manque les guillemets. La commande doit etre de la forme add chemin \"blabla\".\n");
        return;
    };

    int nf = longueur((*f).contenu);
    if (p == -1) {
        nc = 0;
    };
    if (nc+nf > TAILLE_FICHIER - 1){
        printf("Erreur: ajouter cette chaine de caractere au fichier le rendrait trop long. Ainsi, elle a ete tronquee.\n");
        return;
    };
    int i = 0;
    while (i < nc && nf+i < TAILLE_FICHIER - 1){
        (*f).contenu[nf+i] = chaine[p+2+i];
        i++;
    };
    (*f).contenu[nf+i] = '\0';
};

int find_int(char chaine[]){
    int res = 0;
    int i = 0;
    while (chaine[i]!='\0'){
        res = res*10;
        switch(chaine[i])
        {
            case '0': res = res + 0; break;
            case '1': res = res + 1; break;
            case '2': res = res + 2; break;
            case '3': res = res + 3; break;
            case '4': res = res + 4; break;
            case '5': res = res + 5; break;
            case '6': res = res + 6; break;
            case '7': res = res + 7; break;
            case '8': res = res + 8; break;
            case '9': res = res + 9; break;
            case '\0': break;
            default:  printf("Erreur: Commande invalide, dans cut -k chemin-vers-fichier, k doit etre un nombre entier positif.\n");
                return -1;
        }
        i++;
    };
    return res;
};

void cut(char chaine[]){
/* cut -all fifi vide le fichier fifi, cut -k fifi retire les k derniers caracteres,
où k est specifie par l'utilisateur/rice*/
    int p = prochaine_position(chaine,0,' ');
    if (p < 1 || chaine[0]!='-'){
        printf("Erreur: Commande invalide. Essayez cut -all chemin-fichier pour vider le fichier, ou cut -k chemin-fichier ou k est un entier pour retirer les k derniers caracteres du fichier.\n");
        return;
    };
    char *chemin = chaine + p + 1;
    fichier* f = curr_dir;
    f = follow_path(chemin);
    if (f==&error){
        return;
    };
    char *commande = chaine;
    commande = lire(chaine,1,p);
    if (commande==error_chain){
        return;
    };
    if (sont_egales(commande,"all")){
        (*f).contenu[0]='\0';
        return;
    };
    int k = 0;
    k = find_int(commande);
    if (k == -1){
        return;
    };
    int n = longueur((*f).contenu);
    if (n-k >= 0){
        (*f).contenu[n-k]='\0';
    };
    if (n-k < 0){
        (*f).contenu[0]='\0';
    };
    return;
};

void entree_clavier(char entree[]){
/* Lorsque l'utilisateur a saisi sa commande suivi de "entree", cette fonction-la va
etre appelee. Elle doit lire les premiers caracteres de la chaine (jusqu'a l'espace), et faire appel
a l'une des fonctions du shell. Si les caracteres jusqu'au premier espace ne constituent pas un nom de
commande, renvoyer un message d'erreur (dans l'ideal, quelque chose comme "Erreur: echi ne constitue pas 
un nom de commande valide." si l'utilisateur entre "echi home/fifi". */
    int n = longueur(entree);
    if (n == 0){
        return;
    };
    int p = prochaine_position(entree,0,' ');
    if (p > 5){
        printf("Erreur: Verifiez que vous n'avez pas oublie d'espace, aucun nom de commande n'est aussi long.\n");
    };
    char *commande = entree;
    if (p != -1) {
        commande = lire(entree,0,p);
        if (commande==error_chain){
            return;
        };
    };
    if (sont_egales(commande,"echo")){
        echo(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"ls")){
        if (p == -1 || p + 1 == n){
            ls("");
            return;
        };
        ls(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"pwd")){
        if (!(sont_egales(entree,"pwd"))){
            printf("Erreur: pwd est une commande qui ne prend pas d'arguments. Verifiez egalement que vous n'avez pas rajoute des espaces inutiles.\n");
            return;
        };
        pwd();
        return;
    };
    if (sont_egales(commande,"cd")){
        if (p == -1 || p + 1 == n){
            printf("Erreur: cd doit prendre un chemin (eventuellement comprenant ..) vers un dossier comme argument\n");
            return;
        };
        cd(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"touch")){
        if (p == -1 || p + 1 == n){
            printf("Erreur: veuillez donner un nom au fichier que vous voulez creer\n");
            return;
        };
        touch(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"mkdir")){
        if (p == -1 || p + 1 == n){
            printf("Erreur: veuillez donner un nom au dossier que vous voulez creer\n");
            return;
        };
        mkdir(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"rm")){ /*rm -r chemin pour supprimer ce qui est au bout du chemin, que ce soit un dossier (vide ou plein), ou un fichier*/
        if (entree[3] == '-' && entree[4] == 'r' && entree[5] == ' '){
            rm_r(entree + 6);
            return;
        };
        if (p == -1 || p + 1 == n){
            rm_fichier("");
            return;
        };
        rm_fichier(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"cp")){ /*cp -d dossier chemin-arrivee pour copier le dossier dans chemin-arrivee. chemin-arrivee peut-etre remplace par un '.' si c'est le dossier courant*/
        if (entree[3] == '-' && entree[4] == 'd' && entree[5] == ' '){
            cp_d(entree + 6);
            return;
        };
        cp_fichier(entree + 3);
        return;
    };
    if (sont_egales(commande,"mv")){ /*mv -d dossier chemin-arrivee pour deplacer le dossier dans chemin-arrivee. chemin-arrivee peut-etre remplace par un '.' si c'est le dossier courant*/
        if (entree[3] == '-' && entree[4] == 'd' && entree[5] == ' '){
            mv_d(entree + 6);
            return;
        };
        mv_fichier(entree + 3);
        return;
    };
    if (sont_egales(commande,"add")){
        add(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"cut")){
        cut(entree + p + 1);
        return;
    };
    printf("Erreur: %s n'est pas un nom de commande valide.\n",commande);
};
