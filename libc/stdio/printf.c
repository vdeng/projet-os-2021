#include <limits.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

static bool print(const char* data, size_t length) {
	const unsigned char* bytes = (const unsigned char*) data;
	for (size_t i = 0; i < length; i++)
		if (putchar(bytes[i]) == EOF)
			return false;
	return true;
}

static int numdigits(const int i) {
	if(i == 0)
		return 1;
	int j = i;
	int numdig = 0;
	while(j != 0) {
		j /= 10;
		numdig += 1;
	}
	return numdig;
}

static int num_hexdigits(const unsigned int i) {
	if(i == 0)
		return 1;
	unsigned int j = i;
	int numdig = 0;
	while(j != 0) {
		j >>= 4;
		numdig += 1;
	}
	return numdig;
}

static int abs(const int i) {
	if(i < 0)
		return -i;
	return i;
}

int printf(const char* restrict format, ...) {
	va_list parameters;
	va_start(parameters, format);

	int written = 0;

	while (*format != '\0') {
		size_t maxrem = INT_MAX - written;

		if (format[0] != '%' || format[1] == '%') {
			if (format[0] == '%')
				format++;
			size_t amount = 1;
			while (format[amount] && format[amount] != '%')
				amount++;
			if (maxrem < amount) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			if (!print(format, amount))
				return -1;
			format += amount;
			written += amount;
			continue;
		}

		const char* format_begun_at = format++;

		if (*format == 'c') {
			format++;
			char c = (char) va_arg(parameters, int /* char promotes to int */);
			if (!maxrem) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			if (!print(&c, sizeof(c)))
				return -1;
			written++;
		} else if (*format == 's') {
			format++;
			const char* str = va_arg(parameters, const char*);
			size_t len = strlen(str);
			if (maxrem < len) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			if (!print(str, len))
				return -1;
			written += len;
		} else if (*format == 'd') {
			format++;
			int i = va_arg(parameters, int);
			char str[11];
			int pos = 0;
			if(i < 0)
				str[pos++] = '-';
			int numdig = numdigits(i);
			for(int idig = 0; idig < numdig; idig++) {
				str[pos+numdig-1-idig] = '0' + abs(i % 10);
				i /= 10;
			}
			str[pos+numdig] = '\0';
			if(!print(str, pos+numdig))
				return -1;
			written += pos+numdig;
		} else if(*format == 'x') {
			format++;
			unsigned int i = va_arg(parameters, unsigned int);
			char str[10];
			int pos = 0;
			int numdig = num_hexdigits(i);
			for(int idig = 0; idig < numdig; idig++) {
				unsigned int dig = abs(i % 16);
				if(dig >= 10)
					str[pos+numdig-1-idig] = 'A' + (dig - 10);
				else
					str[pos+numdig-1-idig] = '0' + dig;
				i >>= 4;
			}
			str[pos+numdig] = '\0';
			if(!print(str, pos+numdig))
				return -1;
			written += pos+numdig;
		} else {
			format = format_begun_at;
			size_t len = strlen(format);
			if (maxrem < len) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			if (!print(format, len))
				return -1;
			written += len;
			format += len;
		}
	}

	va_end(parameters);
	return written;
}
