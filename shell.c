#include <stdio.h> /* Pas accessible dans l'OS, mais on va utiliser la fonction printf de cette bibliothèque
pour compiler le code et éventuellement le tester. Dans l'OS la fonction printf de cette bibliothèque sera 
remplacée par celle de Victor.*/
#include <stdlib.h> /* On en a besoin pour pouvoir utiliser malloc et atoi */
#define TAILLE_DOSSIER 10
#define TAILLE_NOM 21 /*20 = taille des chaînes de caractères utilisées pour les noms de fichier
                        Le 21 permet de s'assurer de la présence de '\0' à la fin de la chaîne de charactères*/
#define TAILLE_FICHIER 100 
#define TAILLE_SYSTEME 100 /*Indique le nombre total de fichiers/dossiers qui peuvent se trouver dans le système de fichiers */

enum type_fichier { Fichier, Dossier };
typedef struct fichier_s {
    enum type_fichier type;
    char nom[TAILLE_NOM];
    struct fichier_s* pere;
    union {
        struct fichier_s *sous_dossier[TAILLE_DOSSIER];
        char contenu[TAILLE_FICHIER];
    };
} fichier ;

fichier home = {.type = Dossier, .nom="home"};
/* Je ne sais pas si c'est le bon endroit, mais il faudra qu'au lancement du shell soit créé un dossier home
étant son propre père et de contenu vide.*/
fichier* curr_dir = &home;
/* curr_dir est une variable globale, un pointeur vers le dossier courant. Au début, c'est home.*/
fichier systeme_de_fichiers[TAILLE_SYSTEME];
int nb_df = 0;

int longueur(char chaine[]){
    int n = 0;
    char caractereActuel = 0;
    do
    {
        caractereActuel = chaine[n];
        n++;    
    }
    while(caractereActuel != '\0'); // On boucle tant qu'on n'est pas arrivé à la fin de la chaine
    n--;
    return n;
};

int sont_egales(char chaine1[],char chaine2[]){
    int i = 0;
    while (chaine1[i] == chaine2[i] && chaine1[i] != '\0' && chaine2[i] != '\0'){
        i += 1;
    };
    if (!(chaine1[i] == '\0' && chaine2[i] == '\0')){
        return 0;
    };
    return 1;
};

int prochaine_position(char chaine[],int d,char c){
    int f = d;
    char caractereActuel = 0;
    do
    {
        caractereActuel = chaine[f];
        f++;    
    }
    while(caractereActuel != '\0' && caractereActuel != c); // On boucle tant qu'on n'est pas arrivé à la fin de la chaine ou qu'on n'a pas trouvé c
    if (caractereActuel == '\0'){
        return -1;
    };
    f--;
    return f;
};

char *lire(char chaine[],int d,int f){ /*L'élément en f n'est pas pris en compte*/
    char *sous_chaine = NULL;
    sous_chaine = malloc (sizeof (*sous_chaine) * (f - d + 1));
    for (int i = 0; i < f-d ;i++){
        sous_chaine[i] = chaine[d+i];
    };
    sous_chaine[f-d] = '\0';
    return(sous_chaine);
};

fichier* search_child(fichier* dir, char fils[]){
/* Une fonction qui prend un pointeur vers un dossier dir et un nom de fils et renvoie, s'il le trouve, un
pointeur vers un fichier fils de dir ayant le nom requis. Cette fonction servira pour la fonction auxiliaire
suivante. Il faut s'arranger pour que le message d'erreur souhaité soit affiché si on l'appelle depuis 
follow_path.*/
    if (longueur(fils)==0){
        printf("Erreur: le chemin est invalide\n");
        exit(1);
    };
    fichier f_curr = *dir;
    if (sont_egales(fils,"..")){
        if (f_curr.type == Fichier){
            printf("Erreur: %s n'est pas un Dossier.\n",f_curr.nom);
            exit(1);
        };
        if (dir == &home){
            printf("Erreur: le dossier racine home n'a pas de dossier parent.\n");
            exit(1);
        };
        return f_curr.pere;
    };
    for (int i = 0 ; i < TAILLE_DOSSIER ; i++)
        {
            if (f_curr.sous_dossier[i] != 0) {
                fichier f = *f_curr.sous_dossier[i] ;
                if (sont_egales(f.nom,fils)){
                    return f_curr.sous_dossier[i];
                };
            };
        }
    printf("Erreur: Le dossier %s ne contient pas de fichier ou dossier %s.\n",f_curr.nom,fils);
    exit(1);
};

fichier* follow_path(char chemin[]){
/* Une fonction qui servira pour plusieurs des commandes ci-dessous. Elle prend une chaîne de caractères 
censée représenter un chemin et renvoie un pointeur vers le fichier (fichier texte ou dossier) destination
du chemin. Pour ce faire, elle lit la chaîne jusqu'à tomber sur un slash 
Sinon, elle renvoie un message d'erreur. Dans l'idéal, ce message d'erreur précise le premier nom de fichier/
dossier pas trouvé. Par exemple, si on entre "cd users/cloti/tests, et qu'il existe un dossier users dans
home, mais pas de dossier cloti dans users, on renvoie "Erreur: le dossier users ne contient pas de fichier
ou dossier cloti."*/
    int n = longueur(chemin);
    int d = 0;
    int d2 = 0;
    fichier* f = curr_dir;
    char *fils = NULL;
    while(n-d>0){
        d2 = prochaine_position(chemin,d,'/');
        if (d2 == -1){
            fils = chemin + d;
            d = n;
        }
        else {
            fils = lire(chemin,d,d2);
            d = d2 + 1;
        };
        f = search_child(f,fils);
    };
    return f;
};

/* Il faudra sans doute d'autres fonctions auxiliaires.*/

/* A partir de là, c'est les commandes.
Question: en bash, on a souvent la possibilité de passer plusieurs arguments (ex: créer plusieurs fichiers
d'un coup: touch fifi1 fifi2...) Est-ce qu'on fait pareil ou pas? Cela peut rendre le code plus compliqué.*/

void echo(char chaine[]){
/* chaine peut être un chemin vers le fichier dont l'utilisateur veut afficher le contenu, ou une chaine de
caractère (si l'utilisateur a saisi "echo "Coucou"", chaine est une chaîne de caractère dont le premier 
caractère est: " ). Il y a donc 2 cas à traiter.*/
    int n = longueur(chaine);
    if (chaine[0] == '"'){
        if (chaine[n-1] != '"' || (n-1) == 0){
            printf("Erreur: Argument invalide. echo prend comme argument soit un chemin vers un fichier, soit une chaîne de caractères.\n");
            exit(1);
        };
        char *a_afficher = NULL;
        a_afficher = lire(chaine,1,n-1);
        printf("%s\n",a_afficher);
        return;
    };
    fichier f = home;
    f = *(follow_path(chaine));
    if (f.type != Fichier){
        printf("Erreur: echo n'affiche pas le contenu d'un dossier, mais seulement d'un fichier.\n");
        exit(1);
    };
    printf("%s\n",f.contenu);
    return;
};

void ls(char chemin[]) {
    fichier* f = follow_path(chemin);
    fichier f_curr = *f;
    if (f_curr.type == Fichier){
        printf("Erreur: %s n'est pas un dossier.\n",f_curr.nom);
        exit(1);
    };
    printf("Dossiers :\n");
    for (int i = 0 ; i < TAILLE_DOSSIER ; i++)
        {
            if (f_curr.sous_dossier[i] != 0) {
                fichier f = *f_curr.sous_dossier[i] ;
                if (f.type == Dossier){
                    printf("%s ",f.nom);
                };
            };
        }
    printf("\n");
    printf("Fichiers :\n");
    for (int i = 0 ; i < TAILLE_DOSSIER ; i++)
        {
            if (f_curr.sous_dossier[i] != 0) {
                fichier f = *f_curr.sous_dossier[i] ;
                if (f.type == Fichier){
                    printf("%s ",f.nom);
                };
            };
        }
    printf("\n");
}; /* On pourrait améliorer cette fonction en rajoutant un tri par ordre alphabétique*/

void path(fichier* f) {
/* Fonction auxiliaire crée pour faciliter le retour à la ligne après pwd */
    fichier fi = *f;
    if (f != &home){
        path(fi.pere);
    };
    printf("/%s",fi.nom);
};

void pwd() {
/* On suppose qu'on a déjà vérifié que la chaîne de caractères était "pwd" exactement.
Cette vérification est faite de sorte à s'assurer que l'utilisateur/utilisatrice commprenne
que pwd ne prend pas d'argument et indique seulement le chemin du dossier actuel.*/
    path(curr_dir);
    printf("\n");
};

void cd(char chemin[]){
    fichier* f = follow_path(chemin);
    fichier f_curr = *f;
    if (f_curr.type == Fichier){
        printf("Erreur: %s n'est pas un dossier.\n",f_curr.nom);
        exit(1);
    };
    curr_dir = f;
    return;
};

int nom_valide(fichier f,char nom[]){
/*Un nom valide est un nom nom vide qui n'est pas trop long, n'a ni espace ni slash, et ne corresponde pas déjà à un
nom de dossier ou de fichier contenu dans le fichier f.*/
    if (longueur(nom) > TAILLE_NOM-1){
        printf("Erreur: %s est un nom trop long. Un nom de fichier ou de dossier doit être d'au plus %d caractères.\n",nom,TAILLE_NOM-1);
        exit(1);
    };
    if (longueur(nom) == 0){
        printf("Erreur: Veuillez donner un nom au fichier ou dossier que vous souhaitez créer.\n");
        exit(1);
    };
    if (prochaine_position(nom,0,' ') != -1){
        printf("Erreur: Un nom de fichier ou de dossier ne peut pas contenir d'espace.\n");
        exit(1);
    };
    if (prochaine_position(nom,0,'/') != -1){
        printf("Erreur: Un nom de fichier ou de dossier ne peut pas contenir de slash '/'.\n");
        exit(1);
    };
    if (prochaine_position(nom,0,'"') != -1){
        printf("Erreur: Un nom de fichier ou de dossier ne peut pas contenir de guillemets ' \" '.\n");
        exit(1);
    };
    for (int i = 0 ; i < TAILLE_DOSSIER ; i++){
        if (f.sous_dossier[i] != 0) {
            fichier fils = *f.sous_dossier[i] ;
            if (sont_egales(fils.nom,nom)){
                printf("Erreur: Il y a déjà un fichier ou un dossier de nom %s dans %s.\n",nom,f.nom);
                exit(1);
            };
        };
    };
    return 1;
};

int emplacement_libre(fichier f){
    for (int i = 0 ; i < TAILLE_DOSSIER ; i++){
        if (f.sous_dossier[i] == 0) {
            return i;
        };
    };
    return -1;
}

void touch(char chemin[]){
/* Crée un fichier vide dont le chemin à partir du dossier courant sera celui passé en paramètre.
Seul le fichier peut-être créé: les dossiers intermédiaires du chemin doivent déjà exister.*/
    int n = longueur(chemin);
    int d = 0;
    int d2 = 0;
    fichier* f = curr_dir;
    char *fils = NULL;
    d2 = prochaine_position(chemin,d,'/');
    while(d2 != -1){
        fils = lire(chemin,d,d2);
        d = d2 + 1;
        f = search_child(f,fils);
        d2 = prochaine_position(chemin,d,'/');
    };
    char *nom = chemin;
    nom = chemin + d;

    if ((*f).type == Fichier){
        printf("Erreur: %s est un fichier, on ne peut pas créer de fichier à l'intérieur d'un fichier.\n",(*f).nom);
        exit(1);
    };
    

    if (nom_valide((*f),nom)){
        int r = emplacement_libre((*f));
        if (r == -1){
            printf("Erreur: Le dossier %s est déjà plein.\n",(*f).nom);
            exit(1);
        };
        if (nb_df + 1 == TAILLE_SYSTEME){
            printf("Erreur: le système de fichiers est plein.\n");
            exit(1);
        };
        systeme_de_fichiers[nb_df].type = Fichier;
        systeme_de_fichiers[nb_df].pere = f;
        systeme_de_fichiers[nb_df].contenu[0] = '\0';
        int i = 0;
        while (nom[i] != '\0'){
            systeme_de_fichiers[nb_df].nom[i] = nom[i];
            i++;
        };
        (*f).sous_dossier[r] = &systeme_de_fichiers[nb_df];
        nb_df++;
    };
};

void mkdir(char chemin[]){
/* Idem mais pour un dossier.*/
    int n = longueur(chemin);
    int d = 0;
    int d2 = 0;
    fichier* f = curr_dir;
    char *fils = NULL;
    d2 = prochaine_position(chemin,d,'/');
    while(d2 != -1){
        fils = lire(chemin,d,d2);
        d = d2 + 1;
        f = search_child(f,fils);
        d2 = prochaine_position(chemin,d,'/');
    };
    char *nom = chemin;
    nom = chemin + d;

    if ((*f).type == Fichier){
        printf("Erreur: %s est un fichier, on ne peut pas créer de dossier à l'intérieur d'un fichier.\n",(*f).nom);
        exit(1);
    };
    

    if (nom_valide((*f),nom)){
        int r = emplacement_libre((*f));
        if (r == -1){
            printf("Erreur: Le dossier %s est déjà plein.\n",(*f).nom);
            exit(1);
        };
        if (nb_df + 1 == TAILLE_SYSTEME){
            printf("Erreur: le système de fichiers est plein.\n");
            exit(1);
        };
        systeme_de_fichiers[nb_df].type = Dossier;
        systeme_de_fichiers[nb_df].pere = f;
        systeme_de_fichiers[nb_df].sous_dossier[0] = NULL;
        int i = 0;
        while (nom[i] != '\0'){
            systeme_de_fichiers[nb_df].nom[i] = nom[i];
            i++;
        };
        (*f).sous_dossier[r] = &systeme_de_fichiers[nb_df];
        nb_df++;
    };
};

int dossier_non_vide(fichier* f){
    if ((*f).type == Fichier){
        return -1;
    }
    for (int i = 0; i < TAILLE_DOSSIER; i++){
        if ((*f).sous_dossier[i] != 0){
            return i;
        };
    }
    return -1;
};

void rm(fichier* f){
    int r = dossier_non_vide(f);
    if (r != -1){
        rm((*f).sous_dossier[r]);
        rm(f);
        return;
    };
    int i = 0;
    while (i<nb_df && &(systeme_de_fichiers[i]) != f ){
        i++;
    };
    if (i==nb_df){
        printf("On ne peut pas effacer le dossier racine.\n");
        exit(1);
    };
    fichier* pere = (*f).pere;
    for (int i = 0; i < TAILLE_DOSSIER; i++){
        if ((*pere).sous_dossier[i] == f){
            (*pere).sous_dossier[i] = 0;
        };
    }
    systeme_de_fichiers[i] = systeme_de_fichiers[nb_df-1];
    nb_df--;
};

void rm_fichier(char chemin[]){
    fichier* f = curr_dir;
    f = follow_path(chemin);
    if ((*f).type == Dossier){
        printf("Erreur: %s est un dossier. Si vous voulez vraiment le supprimer, faites rm -d %s.\n",(*f).nom,chemin);
        exit(1);
    };
    rm(f);
};

void rm_r(char chemin[]){
/* rm fifi détruit le (vrai) fichier nommé fifi s'il en existe un dans le dossier courant et 
renvoie un message d'erreur sinon. rm didi, où didi est un dossier, renvoie un message d'erreur disant qu'on
ne peut supprimer un dossier. rm -r didi où didi est un dossier et rm -r fifi marchent.
Il y a aussi des options qui permettent de détruire un dossier non vide. Il faut voir si on veut coller à 
bash ou faire un truc plus simple. Je pense qu'il est indispensable d'offrir la possibilité de supprimer des
dossiers, d'une manière ou d'une autre. */
    fichier* f = curr_dir;
    f = follow_path(chemin);
    rm(f);
};

/* Ce qu'il manque, c'est des fonctions pour modifier les fichiers. En bash, cela ne se fait pas si
simplement, on utilise des trucs sophistiqués comme cat > fifi. Il faut donc inventer des commandes de 
spécifiques à notre OS. Je propose les suivantes.*/

void add(char chaine[]){
/*add chemin_fifi "blabla" ajoute blabla à la fin du fichier fifi */
    int p = prochaine_position(chaine,0,' ');
    char *chemin = chaine;
    if (p != -1) {
        chemin = lire(chaine,0,p);
    };
    fichier* f = curr_dir;
    f = follow_path(chemin);
    if ( (*f).type != Fichier ){
        printf("Erreur: %s n'est pas un fichier\n",(*f).nom);
        exit(1);
    };
    int nc = longueur(chaine) - p - 3;

    int nf = longueur((*f).contenu);
    if (p == -1) {
        nc = 0;
    };
    if (nc+nf > TAILLE_FICHIER - 1){
        printf("Erreur: ajouter cette chaine de caractère au fichier le rendrait trop long. Ainsi, elle a été tronquée.\n");
        exit(1);
    };
    int i = 0;
    while (i < nc && nf+i < TAILLE_FICHIER - 1){
        (*f).contenu[nf+i] = chaine[p+2+i];
        i++;
    };
    (*f).contenu[nf+i] = '\0';
};

void cut(char chaine[]){
/* cut -all fifi vide le fichier fifi, cut -k fifi retire les k derniers caractères,
où k est spécifié par l'utilisateur/rice*/
    int p = prochaine_position(chaine,0,' ');
    if (p < 1 || chaine[0]!='-'){
        printf("Erreur: Commande invalide. Essayez cut -all chemin-fichier pour vider le fichier, ou cut -k chemin-fichier ou k est un entier pour retirer les k derniers caractères du fichier.\n");
        exit(1);
    };
    char *chemin = chaine + p + 1;
    fichier* f = curr_dir;
    f = follow_path(chemin);
    char *commande = chaine;
    commande = lire(chaine,1,p);
    if (sont_egales(commande,"add")){
        (*f).contenu[0]='\0';
        return;
    };
    int k = 0;
    k = atoi(commande);
    int n = longueur((*f).contenu);
    if (n-k >= 0){
        (*f).contenu[n-k]='\0';
    };
    if (n-k < 0){
        (*f).contenu[0]='\0';
    };
    return;
};

/*On peut maintenant faire cp et donc mv*/

void cp(fichier* fi, fichier* da){
    fichier f = home;
    f = *fi;
    if (f.type == Fichier){
        if (nom_valide((*da),f.nom)){
            int r = emplacement_libre((*da));
            if (r == -1){
                printf("Erreur: Le dossier %s est déjà plein.\n",(*da).nom);
                exit(1);
            };
            if (nb_df + 1 == TAILLE_SYSTEME){
                printf("Erreur: le système de fichiers est plein.\n");
                exit(1);
            };
            systeme_de_fichiers[nb_df].type = Fichier;
            systeme_de_fichiers[nb_df].pere = da;
            int k = 0;
            while(f.contenu[k] != '\0'){
                systeme_de_fichiers[nb_df].contenu[k] = f.contenu[k];
                k++;
            }
            systeme_de_fichiers[nb_df].contenu[k] = '\0';
            int i = 0;
            while (f.nom[i] != '\0'){
                systeme_de_fichiers[nb_df].nom[i] = f.nom[i];
                i++;
            };
            (*da).sous_dossier[r] = &systeme_de_fichiers[nb_df];
            nb_df++;
        };
        return;
    };

    if (nom_valide((*da),f.nom)){
        int r = emplacement_libre((*da));
        if (r == -1){
            printf("Erreur: Le dossier %s est déjà plein.\n",(*da).nom);
            exit(1);
        };
        if (nb_df + 1 == TAILLE_SYSTEME){
            printf("Erreur: le système de fichiers est plein.\n");
            exit(1);
        };
        systeme_de_fichiers[nb_df].type = Dossier;
        systeme_de_fichiers[nb_df].pere = da;
        systeme_de_fichiers[nb_df].sous_dossier[0] = NULL;
        int i = 0;
        while (f.nom[i] != '\0'){
            systeme_de_fichiers[nb_df].nom[i] = f.nom[i];
            i++;
        };
        (*da).sous_dossier[r] = &systeme_de_fichiers[nb_df];
        nb_df++;
    };
    for (int h = 0; h< TAILLE_DOSSIER; h++){
        if (f.sous_dossier[h] != 0){
            cp(f.sous_dossier[h],&systeme_de_fichiers[nb_df]);
        };
    };
};

void cp_fichier(char chaine[]){
    int p = prochaine_position(chaine,0,' ');
    if (p == -1){
        printf("Erreur: Commande invalide. Essayez cp chemin-fichier chemin2 pour copier un fichier dans chemin2, et cp -d chemin-fichier chemin2 pour copier un fichier ou dossier dans chemin2. Si le dossier d'arrivée est le dossier courant, chemin2 peut être remplacé par un simple point '.'.\n");
        exit(1);
    };
    char *chemin2 = chaine + p + 1;
    char *chemin1 = chaine;
    chemin1 = lire(chaine,0,p);
    fichier* fi = curr_dir;
    fi = follow_path(chemin1);
    if ((*fi).type != Fichier){
        printf("Erreur: Commande invalide. Essayez cp -d chemin-fichier chemin2 pour copier un fichier ou dossier dans chemin2. Si le dossier d'arrivée est le dossier courant, chemin2 peut être remplacé par un simple point '.'.\n");
        exit(1);
    };
    fichier* da = curr_dir;
    if (!(sont_egales(chemin2,"."))){
        da = follow_path(chemin2);
    };
    if ((*da).type != Dossier){
        printf("Erreur: Commande invalide. %s n'est pas un dossier d'arrivée valide.\n",(*da).nom);
        exit(1);
    };
    cp(fi,da);
};

void cp_d(char chaine[]){
/* En bash cp chemin_fifi chemin_didi copie le fichier dont le chemin est chemin_fifi vers le dossier dont le
chemin est chemin_didi. Erreur si chemin_fifi est un chemin vers un dossier. cp -d chemin_didi1 chemin_didi2
copie didi1 et son contenu dans didi2. A nouveau, il faudra voir ce qu'on veut faire exactement.*/
/* Il faut voir ce qu'on fait du point ou des deux points (cp didi/fifi . copie fifi dans le
dossier courant en bash.*/
    int p = prochaine_position(chaine,0,' ');
    if (p == -1){
        printf("Erreur: Commande invalide. Essayez cp chemin-fichier chemin2 pour copier un fichier dans chemin2, et cp -d chemin-fichier chemin2 pour copier un fichier ou dossier dans chemin2. Si le dossier d'arrivée est le dossier courant, chemin2 peut être remplacé par un simple point '.'.\n");
        exit(1);
    };
    char *chemin2 = chaine + p + 1;
    char *chemin1 = chaine;
    chemin1 = lire(chaine,0,p);
    fichier* fi = curr_dir;
    fi = follow_path(chemin1);
    fichier* da = curr_dir;
    if (!(sont_egales(chemin2,"."))){
        da = follow_path(chemin2);
    };
    if ((*da).type != Dossier){
        printf("Erreur: Commande invalide. %s n'est pas un dossier d'arrivée valide.\n",(*da).nom);
        exit(1);
    };
    cp(fi,da);
};

void mv_fichier(char chaine[]){
    int p = prochaine_position(chaine,0,' ');
    if (p == -1){
        printf("Erreur: Commande invalide. Essayez mv chemin-fichier chemin2 pour déplacer un fichier dans chemin2, et mv -d chemin-fichier chemin2 pour déplacer un fichier ou dossier dans chemin2. Si le dossier d'arrivée est le dossier courant, chemin2 peut être remplacé par un simple point '.'.\n");
        exit(1);
    };
    char *chemin2 = chaine + p + 1;
    char *chemin1 = chaine;
    chemin1 = lire(chaine,0,p);
    fichier* fi = curr_dir;
    fi = follow_path(chemin1);
    if ((*fi).type != Fichier){
        printf("Erreur: Commande invalide. Essayez mv -d chemin-dossier chemin2 pour déplacer un dossier dans chemin2.\n");
        exit(1);
    };
    fichier* da = curr_dir;
    if (!(sont_egales(chemin2,"."))){
        da = follow_path(chemin2);
    };
    if ((*da).type != Dossier){
        printf("Erreur: Commande invalide. %s n'est pas un dossier d'arrivée valide.\n",(*da).nom);
        exit(1);
    };

    if (nom_valide((*da),(*fi).nom)){
        int r = emplacement_libre((*da));
        if (r == -1){
            printf("Erreur: Le dossier %s est déjà plein.\n",(*da).nom);
            exit(1);
        };
        (*da).sous_dossier[r] = fi;
    };

    fichier* pere = curr_dir;
    pere = (*fi).pere;
    (*fi).pere = da;
    for (int i = 0; i < TAILLE_DOSSIER; i++){
        if ( (*pere).sous_dossier[i] == fi ){
            (*pere).sous_dossier[i] = 0;
        };
    };
};

void mv_d(char chaine[]){
    int p = prochaine_position(chaine,0,' ');
    if (p == -1){
        printf("Erreur: Commande invalide. Essayez mv chemin-fichier chemin2 pour déplacer un fichier dans chemin2, et mv -d chemin-fichier chemin2 pour déplacer un fichier ou dossier dans chemin2. Si le dossier d'arrivée est le dossier courant, chemin2 peut être remplacé par un simple point '.'.\n");
        exit(1);
    };
    char *chemin2 = chaine + p + 1;
    char *chemin1 = chaine;
    chemin1 = lire(chaine,0,p);
    fichier* fi = curr_dir;
    fi = follow_path(chemin1);
    fichier* da = curr_dir;
    if (!(sont_egales(chemin2,"."))){
        da = follow_path(chemin2);
    };
    if ((*da).type != Dossier){
        printf("Erreur: Commande invalide. %s n'est pas un dossier d'arrivée valide.\n",(*da).nom);
        exit(1);
    };

    if (nom_valide((*da),(*fi).nom)){
        int r = emplacement_libre((*da));
        if (r == -1){
            printf("Erreur: Le dossier %s est déjà plein.\n",(*da).nom);
            exit(1);
        };
        (*da).sous_dossier[r] = fi;
    };

    if (fi == &home){
        printf("Erreur: On ne peut pas déplacer le dossier racine.\n");
        exit(1);
    };
    
    fichier* pere = curr_dir;
    pere = (*fi).pere;
    (*fi).pere = da;
    for (int i = 0; i < TAILLE_DOSSIER; i++){
        if ( (*pere).sous_dossier[i] == fi ){
            (*pere).sous_dossier[i] = 0;
        };
    };
};

void entree_clavier(char entree[]){
/* On imagine que, lorsque l'utilisateur a saisi sa commande suivi de "entrée", cette fonction-là va
être appelée. Elle doit lire les premiers caractères de la chaîne (jusqu'à l'espace), et faire appel
à l'une des fonctions suivantes. Si les caractères jusqu'au premier espace ne constituent pas un nom de
commande, renvoyer un message d'erreur (dans l'idéal, quelque chose comme "Erreur: echi ne constitue pas 
un nom de commande valide." si l'utilisateur entre "echi home/fifi". */
    int n = longueur(entree);
    if (n == 0){
        return;
    };
    int p = prochaine_position(entree,0,' ');
    char *commande = entree;
    if (p != -1) {
        commande = lire(entree,0,p);
    };
    if (sont_egales(commande,"echo")){
        echo(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"ls")){
        if (p == -1 || p + 1 == n){
            ls("");
            return;
        };
        ls(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"pwd")){
        if (entree != "pwd"){
            printf("Erreur: pwd est une commande qui ne prend pas d'arguments. Vérifiez également que vous n'avez pas rajouté des espaces inutiles.\n");
            return;
        };
        pwd();
        return;
    };
    if (sont_egales(commande,"cd")){
        if (p == -1 || p + 1 == n){
            printf("Erreur: cd doit prendre un chemin (éventuellement comprenant ..) vers un dossier comme argument\n");
            return;
        };
        cd(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"touch")){
        if (p == -1 || p + 1 == n){
            printf("Erreur: veuillez donner un nom au fichier que vous voulez créer\n");
            return;
        };
        touch(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"mkdir")){
        if (p == -1 || p + 1 == n){
            printf("Erreur: veuillez donner un nom au dossier que vous voulez créer\n");
            return;
        };
        mkdir(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"rm")){ /*rm -r chemin pour supprimer ce qui est au bout du chemin, que ce soit un dossier (vide ou plein), ou un fichier*/
        if (entree[3] == '-' && entree[4] == 'r' && entree[5] == ' '){
            rm_r(entree + 6);
            return;
        };
        rm_fichier(entree + 3);
        return;
    };
    if (sont_egales(commande,"add")){
        add(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"cut")){
        cut(entree + p + 1);
        return;
    };
    if (sont_egales(commande,"cp")){ /*cp -d dossier chemin-arrivee pour copier le dossier dans chemin-arrivee. chemin-arrivee peut-être remplacé par un '.' si c'est le dossier courant*/
        if (entree[3] == '-' && entree[4] == 'd' && entree[5] == ' '){
            cp_d(entree + 6);
            return;
        };
        cp_fichier(entree + 3);
        return;
    };
    if (sont_egales(commande,"mv")){ /*mv -d dossier chemin-arrivee pour déplacer le dossier dans chemin-arrivee. chemin-arrivee peut-être remplacé par un '.' si c'est le dossier courant*/
        if (entree[3] == '-' && entree[4] == 'd' && entree[5] == ' '){
            mv_d(entree + 6);
            return;
        };
        mv_fichier(entree + 3);
        return;
    };
};

void main() { /*des tests*/
    entree_clavier("mkdir fils1");
    entree_clavier("touch fils1/fifi");
    entree_clavier("add fils1/fifi \"ab0123456789\"");
    entree_clavier("echo fils1/fifi");
    entree_clavier("cut -10 fils1/fifi");
    entree_clavier("echo fils1/fifi");
    entree_clavier("mkdir fils2");
    entree_clavier("mkdir fils2/fils3");
    entree_clavier("cp fils1/fifi fils2/fils3");
    entree_clavier("mv -d fils2/fils3 .");
    entree_clavier("ls");
    entree_clavier("echo fils3/fifi");
    return;
    };
